/**
 * @FileName FolderOpServiceTest.java
 * @Package com.fisheax.Service;
 * @Author fisheax
 * @Date 11/9/2016
 */
package com.fisheax.Service;

import com.fisheax.pojo.FolderInfo;
import com.fisheax.service.FolderOpService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

/**
 * @ClassName: FolderOpServiceTest
 * @Description
 * @Author fisheax
 * @Date 11/9/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class FolderOpServiceTest
{
	@Autowired
	private FolderOpService folderOpService;

	/**
	 * @see com.fisheax.mapper.FolderMapper#insertFolderInfo(FolderInfo)
	 */
	@Test
	public void testCreateFolders()
	{
	}

	/**
	 * @see com.fisheax.mapper.FolderMapper#copyFolders(Map)
	 */
	@Test
	public void testCopyFoldersTo()
	{
	}

	/**
	 * @see com.fisheax.mapper.FolderMapper#moveFolders(String[], String)
	 */
	@Test
	public void testMoveFoldersTo()
	{
	}

	/**
	 * @see com.fisheax.mapper.FolderMapper#deleteFolders(String)
	 */
	@Test
	public void testDeleteFolders()
	{
	}

	/**
	 * @see com.fisheax.mapper.FolderMapper#getFilesInDir(String)
	 * @see com.fisheax.mapper.FolderMapper#getSubFoldersById(String)
	 */
	@Test
	public void testListSubFoldersAndFiles()
	{
	}
}
