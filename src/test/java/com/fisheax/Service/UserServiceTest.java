/**
 * @FileName UserServiceTest.java
 * @Package com.fisheax.Service;
 * @Author fisheax
 * @Date 11/9/2016
 */
package com.fisheax.Service;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.UserInfo;
import com.fisheax.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @ClassName: UserServiceTest
 * @Description
 * @Author fisheax
 * @Date 11/9/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class UserServiceTest
{
	@Autowired
	private UserService userService;

	private UserInfo newCommonUserInfo()
	{
		String uuid = StringUtil.uuid();
		UserInfo info = new UserInfo(uuid);
		info.setUsername(uuid);
		info.setPassword(uuid);

		return info;
	}

	@Test
	public void testRegister()
	{
		UserInfo info = newCommonUserInfo();
		Assert.assertTrue(userService.register(info).succeed());
		Assert.assertNotNull(userService.getUserMapper().getUserInfoById(info.getUserId()));
	}

	@Test
	public void testLogin()
	{
		UserInfo info = newCommonUserInfo();
		String username = info.getUsername();
		String password = info.getPassword();
		Assert.assertTrue(userService.register(info).succeed());

		info = newCommonUserInfo();
		info.setUsername(username);
		info.setPassword(password);
		Assert.assertTrue(userService.login(info).succeed());
	}

	@Test
	public void testAuthenticate()
	{
		UserInfo info = newCommonUserInfo();
		Assert.assertTrue(userService.register(info).succeed());
		Assert.assertNotNull(info = userService.getUserMapper().getUserInfoById(info.getUserId()));
		Assert.assertTrue(userService.authenticate(info.getSecret()).succeed());
	}

}
