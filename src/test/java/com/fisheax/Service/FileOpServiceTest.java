/**
 * @FileName FileOpServiceTest.java
 * @Package com.fisheax.Service;
 * @Author fisheax
 * @Date 11/9/2016
 */
package com.fisheax.Service;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.mapper.FileMapper;
import com.fisheax.mapper.FileMapperTest;
import com.fisheax.pojo.FileInfo;
import com.fisheax.service.FileOpService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * @ClassName: FileOpServiceTest
 * @Description
 * @Author fisheax
 * @Date 11/9/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class FileOpServiceTest
{
	@Autowired
	private FileOpService fileOpService;

	private FileInfo newCommonFileInfo()
	{
		FileInfo info = new FileInfo(StringUtil.uuid());
		info.setFileName("file-name-hhh");
		info.setFileSize(12345);
		info.setStoreId("11111111111111111111111111111111");
		info.setFolderId("11111111111111111111111111111111");
		info.setUploadTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		info.setExists(1);

		return info;
	}

	@Test
	public void testCreateFiles()
	{
		// test by manual
	}

	@Test
	public void testDownloadFile()
	{
		// test by manual
	}

	/**
	 * @see FileMapperTest#testCopyFiles()
	 */
	@Test
	public void testCopyFilesTo()
	{
	}

	/**
	 * @see FileMapperTest#testMoveFiles()
	 */
	@Test
	public void testMoveFilesTo()
	{
	}

	/**
	 * @see FileMapperTest#testDeleteFiles()
	 */
	@Test
	public void testDeleteFiles()
	{
	}

	@Test
	public void testRenameFile()
	{
		FileMapper fileMapper = fileOpService.getFileMapper();
		FileInfo file = newCommonFileInfo();
		fileMapper.insertFileInfo(file);
		Assert.assertTrue(fileOpService.renameFile(file.getFileId(), "a-new-filename").succeed());
	}

	/**
	 * @see FileMapperTest#testListFilesInFolder()
	 */
	@Test
	public void testListFiles()
	{
	}
}
