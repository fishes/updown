/**
 * @FileName LogMapperTest.java
 * @Package com.fisheax.logMapper;
 * @Author fisheax
 * @Date 11/6/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.LogInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

/**
 * @ClassName: LogMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/6/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class LogMapperTest
{
	@Autowired
	private LogMapper logMapper;

	private LogInfo newCommonLog()
	{
		String uuid = StringUtil.uuid();
		LogInfo info = new LogInfo(uuid);
		info.setOpId(uuid);
		info.setOpType("upload");
		info.setOpDetail("upload begin in 2016/11/06");
		info.setOpResult(0);
		return info;
	}

	@Test
	public void testInsertLog()
	{
		Assert.assertEquals(logMapper.insertLogInfo(newCommonLog()), 1);
	}

	@Test
	public void testUpdateLog()
	{
		LogInfo info = newCommonLog();
		Assert.assertEquals(logMapper.insertLogInfo(info), 1);
		info.setOpId("11111111111111111111111111111111");
		info.setOpType("download");
		info.setOpDetail("download begin");
		Assert.assertEquals(logMapper.updateLogInfo(info), 1);
	}

	@Test
	public void testDeleteLog()
	{
		LogInfo info = newCommonLog();
		Assert.assertEquals(logMapper.insertLogInfo(info), 1);
		Assert.assertEquals(logMapper.deleteLogInfo(info.getLogId()), 1);
	}

	@Test
	public void testGetLog()
	{
		LogInfo info = newCommonLog();
		Assert.assertEquals(logMapper.insertLogInfo(info), 1);
		info = logMapper.getLog(info.getLogId());
		Assert.assertNotNull(info);
		System.out.println(info);
	}

	@Test
	public void testGetAllLogs()
	{
		List<LogInfo> infos = logMapper.getAllLogs();
		Assert.assertNotNull(infos);
		System.out.println(infos);
	}

	@Test
	public void testGetLogsByTime()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.set(2000, 1, 1);
		Timestamp from = new Timestamp(calendar.getTimeInMillis());
		calendar.set(2030, 1, 1);
		Timestamp to = new Timestamp(calendar.getTimeInMillis());

		List<LogInfo> infos = logMapper.getLogsByTime(from, to);
		Assert.assertNotNull(infos);
		System.out.println(infos);
	}
}
