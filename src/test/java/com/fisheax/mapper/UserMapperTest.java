/**
 * @FileName FileMapperTest.java
 * @Package com.fisheax.fileMapper;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.UserInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @ClassName: FileMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class UserMapperTest
{
	@Autowired
	private UserMapper userMapper;

	private UserInfo newCommonUserInfo()
	{
		UserInfo info = new UserInfo(StringUtil.uuid());
		info.setUsername("username-hhh");
		info.setPassword("password-hhh");
		info.setSalt("hhhh");

		return info;
	}

	@Test
	public void testInsertUserInfo()
	{
		Assert.assertEquals(userMapper.insertUserInfo(newCommonUserInfo()), 1);
	}

	@Test
	public void testDeleteUserInfo()
	{
		UserInfo info = newCommonUserInfo();
		Assert.assertEquals(userMapper.insertUserInfo(info), 1);
		Assert.assertEquals(userMapper.deleteUserInfo(info.getUserId()), 1);
	}

	@Test
	public void updateUserInfo()
	{
		UserInfo info = newCommonUserInfo();
		Assert.assertEquals(userMapper.insertUserInfo(info), 1);

		info.setPassword("password-ttt");
		info.setSalt("ttttt");

		// would not be changed
		info.setUsername("username-ttt");

		Assert.assertEquals(userMapper.updateUserInfo(info), 1);
		Assert.assertNotNull(info = userMapper.getUserInfoById(info.getUserId()));
		Assert.assertEquals(info.getUsername(), "username-hhh");
		Assert.assertEquals(info.getPassword(), "password-ttt");
		Assert.assertEquals(info.getSalt(), "ttttt");
	}

	@Test
	public void testIsUsernameOccupied()
	{
		Assert.assertFalse(userMapper.isUsernameOccupied(StringUtil.uuid()));

		UserInfo info = newCommonUserInfo();
		Assert.assertEquals(userMapper.insertUserInfo(info), 1);
		Assert.assertTrue(userMapper.isUsernameOccupied("username-hhh"));
	}

	@Test
	public void testIsUserExists()
	{
		UserInfo info = newCommonUserInfo();
		info.setUsername(StringUtil.uuid());
		info.setPassword(StringUtil.uuid());

		Assert.assertFalse(userMapper.isUserValid(info));
		Assert.assertEquals(userMapper.insertUserInfo(info), 1);
		Assert.assertTrue(userMapper.isUserValid(info));
	}

	@Test
	public void testIsKeyValid()
	{
		UserInfo info = newCommonUserInfo();
		Assert.assertEquals(userMapper.insertUserInfo(info), 1);
		Assert.assertFalse(userMapper.isKeyValid(info.getSecret()));
		Assert.assertNotNull(info = userMapper.getUserInfoById(info.getUserId()));
		Assert.assertTrue(userMapper.isKeyValid(info.getSecret()));
	}
}
