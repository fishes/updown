/**
 * @FolderName FolderMapperTest.java
 * @Package com.fisheax.folderMapper;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.FileInfo;
import com.fisheax.pojo.FolderInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: FolderMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class FolderMapperTest
{
	@Autowired
	private FolderMapper folderMapper;
	@Autowired
	private FileMapper fileMapper;

	private FileInfo newCommonFileInfo()
	{
		FileInfo info = new FileInfo(StringUtil.uuid());
		info.setFileName("file-name-hhh");
		info.setFileSize(12345);
		info.setStoreId("11111111111111111111111111111111");
		info.setFolderId("00000000000000000000000000000000");
		info.setUploadTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		info.setExists(1);

		return info;
	}

	// 需要有关联的用户, 因为某些操作依赖于初始化后的根目录
	private String makeRootFolderPid()
	{
		String userId = StringUtil.uuid();
		Assert.assertEquals(folderMapper.createRootFolder(userId), 1);
		return folderMapper.getRootFolderInfo(userId).getFolderId();
	}

	private FolderInfo newCommonFolderInfo()
	{
		FolderInfo info = new FolderInfo(StringUtil.uuid());
		info.setFolderPid("00000000000000000000000000000000");
		info.setUserId("12345678123456781234567812345678");
		info.setFolderName("folder-name-hhh");
		info.setExists(1);

		return info;
	}

	private void cleanUp(String ... folderIds)
	{
		// 因为 insertFolderInfo 对应 SQL 中 select 如果出现多个值, 就会报错
		// 在每次测试后都做下清理工作: 把插入的数据删除掉

		if (folderIds != null)
			for (String folderId : folderIds)
				folderMapper.deleteFolderInfoPermanently(folderId);
	}

	@Test
	public void testInsertFolderInfo()
	{
		FolderInfo info = newCommonFolderInfo();
		info.setFolderPid(makeRootFolderPid());
		Assert.assertEquals(folderMapper.insertFolderInfo(info), 1);

		cleanUp(info.getFolderId());
	}

	@Test
	public void testDeleteFolderInfoPermannently()
	{
		FolderInfo info = newCommonFolderInfo();
		info.setFolderPid(makeRootFolderPid());
		Assert.assertEquals(folderMapper.insertFolderInfo(info), 1);
		Assert.assertEquals(folderMapper.deleteFolderInfoPermanently(info.getFolderId()), 1);

		// no need call #cleanUp
	}

	@Test
	public void testDeleteFolderInfo()
	{
		FolderInfo info = newCommonFolderInfo();
		info.setFolderPid(makeRootFolderPid());
		Assert.assertEquals(folderMapper.insertFolderInfo(info), 1);
		folderMapper.deleteFolderInfo(info.getFolderId());
		Assert.assertNotNull(info = folderMapper.getFolderInfo(info.getFolderId()));
		Assert.assertEquals(info.getExists(), 0);

		cleanUp(info.getFolderId());
	}

	@Test
	public void testUpdateFolderInfo()
	{
		FolderInfo info = newCommonFolderInfo();
		info.setFolderPid(makeRootFolderPid());
		Assert.assertEquals(folderMapper.insertFolderInfo(info), 1);

		info.setFolderPid("22222222222222222222222222222222");
		info.setFolderName("folder-name-ttt");
		info.setLevel(1222);
		Assert.assertEquals(folderMapper.updateFolderInfo(info), 1);
		Assert.assertNotNull(info = folderMapper.getFolderInfo(info.getFolderId()));
		Assert.assertEquals(info.getFolderPid(), "22222222222222222222222222222222");
		Assert.assertEquals(info.getFolderName(), "folder-name-ttt");
		Assert.assertEquals(info.getLevel(), 1222);

		cleanUp(info.getFolderId());
	}

	@Test
	public void testGetFolderInfo()
	{
		FolderInfo info = newCommonFolderInfo();
		info.setFolderPid(makeRootFolderPid());
		String folderId = info.getFolderId();
		Assert.assertEquals(folderMapper.insertFolderInfo(info), 1);

		Assert.assertNotNull(info = folderMapper.getFolderInfo(info.getFolderId()));
		Assert.assertEquals(info.getFolderId(), folderId);
		Assert.assertEquals(info.getFolderName(), "folder-name-hhh");
		Assert.assertEquals(info.getLevel(), 1);
		Assert.assertEquals(info.getExists(), 1);

		cleanUp(info.getFolderId());
	}

	private List<FolderInfo> makeSomeFolders()
	{
		// pinfo -- rinfo
		//       -- sinfo
		//          -- tinfo

		FolderInfo pinfo = newCommonFolderInfo();
		pinfo.setFolderPid(makeRootFolderPid());

		FolderInfo rinfo = newCommonFolderInfo();
		rinfo.setFolderPid(pinfo.getFolderId());

		FolderInfo sinfo = newCommonFolderInfo();
		sinfo.setFolderPid(pinfo.getFolderId());

		FolderInfo tinfo = newCommonFolderInfo();
		tinfo.setFolderPid(sinfo.getFolderId());

		Assert.assertEquals(folderMapper.insertFolderInfo(pinfo), 1);
		Assert.assertEquals(folderMapper.insertFolderInfo(rinfo), 1);
		Assert.assertEquals(folderMapper.insertFolderInfo(sinfo), 1);
		Assert.assertEquals(folderMapper.insertFolderInfo(tinfo), 1);

		return Arrays.asList(pinfo, rinfo, sinfo, tinfo);
	}

	@Test
	public void testGetSubFoldersById()
	{
		List<FolderInfo> infos = makeSomeFolders();
		FolderInfo pinfo = infos.get(0);
		FolderInfo rinfo = infos.get(1);
		FolderInfo sinfo = infos.get(2);
		FolderInfo tinfo = infos.get(3);

		Assert.assertNotNull(infos = folderMapper.getSubFoldersById(pinfo.getFolderId()));
		Assert.assertEquals(infos.size(), 2);

		Assert.assertNotNull(infos = folderMapper.getSubFoldersById(rinfo.getFolderId()));
		Assert.assertEquals(infos.size(), 0);

		Assert.assertNotNull(infos = folderMapper.getSubFoldersById(sinfo.getFolderId()));
		Assert.assertEquals(infos.size(), 1);

		Assert.assertNotNull(infos = folderMapper.getSubFoldersById(tinfo.getFolderId()));
		Assert.assertEquals(infos.size(), 0);

		cleanUp(pinfo.getFolderId(), rinfo.getFolderId(), sinfo.getFolderId(), tinfo.getFolderId());
	}

	@Test
	public void testGetFilesInDir()
	{
		FileInfo fileInfo = newCommonFileInfo();
		FolderInfo folderInfo = newCommonFolderInfo();
		folderInfo.setFolderPid(makeRootFolderPid());
		fileInfo.setFolderId(folderInfo.getFolderId());
		FileInfo tempFileInfo;

		Assert.assertEquals(fileMapper.insertFileInfo(fileInfo), 1);
		Assert.assertEquals(folderMapper.insertFolderInfo(folderInfo), 1);
		Assert.assertNotNull(tempFileInfo = folderMapper.getFilesInDir(folderInfo.getFolderId()).get(0));
		Assert.assertEquals(tempFileInfo.getFileId(), fileInfo.getFileId());

		cleanUp(folderInfo.getFolderId());
	}

	@Test
	public void testCopyFolders()
	{
		// pinfo -- rinfo
		//       -- sinfo
		//          -- tinfo

		List<FolderInfo> folderInfos = makeSomeFolders();
		FolderInfo pinfo = folderInfos.get(0);
		FolderInfo rinfo = folderInfos.get(1);
		FolderInfo sinfo = folderInfos.get(2);
		FolderInfo tinfo = folderInfos.get(3);

		Assert.assertEquals(folderMapper.getSubFoldersById(rinfo.getFolderId()).size(), 0);
		Assert.assertEquals(folderMapper.getSubFoldersById(sinfo.getFolderId()).size(), 1);
		folderMapper.copyFolders(new HashMap<String, Object>(){{
			put("folderPid", rinfo.getFolderId());
			put("folderIds", tinfo.getFolderId());
		}});
		Assert.assertEquals(folderMapper.getSubFoldersById(rinfo.getFolderId()).size(), 1);
		Assert.assertEquals(folderMapper.getSubFoldersById(sinfo.getFolderId()).size(), 1);
	}

	@Test
	public void testDeleteFolders()
	{
		Object[] objects = makeSomeFolders().stream()
			.map(FolderInfo::getFolderId)
			.collect(Collectors.toList()).toArray();
		String[] folderIds = new String[objects.length];
		System.arraycopy(objects, 0, folderIds, 0, folderIds.length);

		folderMapper.deleteFolders(StringUtil.join(folderIds));
		for (String folderId : folderIds)
			Assert.assertEquals(folderMapper.getFolderInfo(folderId).getExists(), 0);
	}

	@Test
	public void testMoveFolders()
	{
		// pinfo -- rinfo
		//       -- sinfo
		//          -- tinfo

		List<FolderInfo> folderInfos = makeSomeFolders();
		FolderInfo pinfo = folderInfos.get(0);
		FolderInfo rinfo = folderInfos.get(1);
		FolderInfo sinfo = folderInfos.get(2);
		FolderInfo tinfo = folderInfos.get(3);

		Assert.assertEquals(folderMapper.getSubFoldersById(rinfo.getFolderId()).size(), 0);
		Assert.assertEquals(folderMapper.getSubFoldersById(sinfo.getFolderId()).size(), 1);
		Assert.assertEquals(folderMapper.moveFolders(new String[]{ tinfo.getFolderId() },
			rinfo.getFolderId()), 1);
		Assert.assertEquals(folderMapper.getSubFoldersById(rinfo.getFolderId()).size(), 1);
		Assert.assertEquals(folderMapper.getSubFoldersById(sinfo.getFolderId()).size(), 0);
	}

	@Test
	public void testGetFolderStructure()
	{
		String userId = StringUtil.uuid();
		Assert.assertEquals(folderMapper.createRootFolder(userId), 1);

		HashMap<String, Object> params = new HashMap<>();
		params.put("userId", userId);
		params.put("filteredFolderIds", "");

		String retVal = folderMapper.getFolderStruture(params);
		System.out.println(retVal);
		System.out.println(params.get("result"));
	}
}
