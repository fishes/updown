/**
 * @FileName FileMapperTest.java
 * @Package com.fisheax.fileMapper;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.StoreInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @ClassName: FileMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class StoreMapperTest
{
	@Autowired
	private StoreMapper storeMapper;

	private StoreInfo newCommonStoreInfo()
	{
		StoreInfo info = new StoreInfo(StringUtil.uuid());
		info.setUserId("11111111111111111111111111111111");
		info.setStoreHash("11111111111111111111111111111111");
		info.setExtName("sql");
		info.setExists(1);
		info.setUpDone(1);

		return info;
	}

	@Test
	public void testInsertStoreInfo()
	{
		Assert.assertEquals(storeMapper.insertStoreInfo(newCommonStoreInfo()), 1);
	}

	@Test
	public void testDeleteStoreInfoPermanently()
	{
		StoreInfo info = newCommonStoreInfo();
		Assert.assertEquals(storeMapper.insertStoreInfo(info), 1);
		Assert.assertEquals(storeMapper.deleteStoreInfoPermanently(info.getStoreId()), 1);
	}

	@Test
	public void testDeleteStoreInfo()
	{
		StoreInfo info = newCommonStoreInfo();
		Assert.assertEquals(storeMapper.insertStoreInfo(info), 1);
		Assert.assertEquals(storeMapper.deleteStoreInfo(info.getStoreId()), 1);
		Assert.assertNotNull(info = storeMapper.getStoreInfo(info.getStoreId()));
		Assert.assertEquals(info.getExists(), 0);
	}
}
