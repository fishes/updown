/**
 * @FileName FileMapperTest.java
 * @Package com.fisheax.fileMapper;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.FileInfo;
import com.fisheax.pojo.FolderInfo;
import com.fisheax.pojo.StoreInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: FileMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class FileMapperTest
{
	@Autowired
	private FileMapper fileMapper;
	@Autowired
	private StoreMapper storeMapper;
	@Autowired
	private FolderMapper folderMapper;

	private FileInfo newCommonFileInfo()
	{
		FileInfo info = new FileInfo(StringUtil.uuid());
		info.setFileName("file-name-hhh");
		info.setFileSize(12345);
		info.setStoreId("11111111111111111111111111111111");
		info.setFolderId("11111111111111111111111111111111");
		info.setUploadTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		info.setExists(1);

		return info;
	}

	private StoreInfo newCommonStoreInfo()
	{
		StoreInfo info = new StoreInfo(StringUtil.uuid());
		info.setUserId("11111111111111111111111111111111");
		info.setStoreHash("11111111111111111111111111111111");
		info.setExtName("sql");
		info.setExists(1);
		info.setUpDone(1);

		return info;
	}

	@Test
	public void testInsertFileInfo()
	{
		Assert.assertEquals(fileMapper.insertFileInfo(newCommonFileInfo()), 1);
	}

	@Test
	public void testCopyFiles()
	{
		StoreInfo store1 = newCommonStoreInfo();
		StoreInfo store2 = newCommonStoreInfo();
		FileInfo file1 = newCommonFileInfo();
		FileInfo file2 = newCommonFileInfo();
		file1.setStoreId(store1.getStoreId());
		file2.setStoreId(store2.getStoreId());

		Assert.assertEquals(fileMapper.insertFileInfo(file1), 1);
		Assert.assertEquals(fileMapper.insertFileInfo(file2), 1);

		Assert.assertEquals(fileMapper.copyFiles(new String[]{file1.getFileId(), file2.getFileId()},
			"22222222222222222222222222222222"), 2);
	}

	@Test
	public void testDeleteFileInfoPermannently()
	{
		FileInfo info = newCommonFileInfo();
		Assert.assertEquals(fileMapper.insertFileInfo(info), 1);
		Assert.assertEquals(fileMapper.deleteFileInfoPermanently(info.getFileId()), 1);
	}

	@Test
	public void testDeleteFileInfo()
	{
		FileInfo info = newCommonFileInfo();
		Assert.assertEquals(fileMapper.insertFileInfo(info), 1);
		fileMapper.deleteFileInfo(info.getFileId());
		Assert.assertNotNull(info = fileMapper.getFileInfo(info.getFileId()));
		Assert.assertEquals(info.getExists(), 0);
	}

	@Test
	public void testDeleteFiles()
	{
		List<FileInfo> fileInfos = Arrays.asList(newCommonFileInfo(), newCommonFileInfo());
		fileInfos.forEach(info -> {
			Assert.assertEquals(fileMapper.insertFileInfo(info), 1);
			fileMapper.deleteFileInfo(info.getFileId());
			Assert.assertNotNull(info = fileMapper.getFileInfo(info.getFileId()));
			Assert.assertEquals(info.getExists(), 0);
		});
	}

	@Test
	public void testDeleteFilesInFolder()
	{
		String userId = StringUtil.uuid();
		Assert.assertEquals(folderMapper.createRootFolder(userId), 1);
		FolderInfo rootFolderInfo = folderMapper.getRootFolderInfo(userId);

		FileInfo file1 = newCommonFileInfo();
		file1.setFolderId(rootFolderInfo.getFolderId());
		FileInfo file2 = newCommonFileInfo();
		file1.setFolderId(rootFolderInfo.getFolderId());

		List<FileInfo> fileInfos = Arrays.asList(file1, file2);
		fileInfos.forEach(info -> {
			Assert.assertEquals(fileMapper.insertFileInfo(info), 1);
		});

		fileMapper.deleteFilesInFolders(new String[]{ rootFolderInfo.getFolderId() });

		fileInfos.forEach(info -> {
			Assert.assertNotNull(info = fileMapper.getFileInfo(info.getFileId()));
			Assert.assertEquals(info.getExists(), 0);
		});
	}

	@Test
	public void testUpdateFileInfo()
	{
		FileInfo info = newCommonFileInfo();
		Assert.assertEquals(fileMapper.insertFileInfo(info), 1);
		info.setFileName("file-name-ttt");
		info.setFolderId("22222222222222222222222222222222");

		// would not be changed
		info.setFileSize(45678);
		info.setStoreId("22222222222222222222222222222222");
		info.setUploadTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		info.setExists(0);

		Assert.assertEquals(fileMapper.updateFileInfo(info), 1);
		Assert.assertNotNull(info = fileMapper.getFileInfo(info.getFileId()));
		Assert.assertEquals(info.getFileName(), "file-name-ttt");
		Assert.assertEquals(info.getFolderId(), "22222222222222222222222222222222");
	}

	@Test
	public void testMoveFiles()
	{
		List<FileInfo> fileInfos = Arrays.asList(newCommonFileInfo(), newCommonFileInfo());
		fileInfos.forEach(fileMapper::insertFileInfo);

		Object[] objects = fileInfos.stream().map(FileInfo::getFileId).collect(Collectors.toList()).toArray();
		String[] fileIds = new String[objects.length];
		System.arraycopy(objects, 0, fileIds, 0, fileIds.length);

		Assert.assertEquals(fileMapper.moveFiles(fileIds, "22222222222222222222222222222222"), fileInfos.size());
	}

	@Test
	public void testDownloadFile()
	{
		FileInfo fileInfo = newCommonFileInfo();
		StoreInfo storeInfo = newCommonStoreInfo();
		fileInfo.setStoreId(storeInfo.getStoreId());

		fileMapper.insertFileInfo(fileInfo);
		storeMapper.insertStoreInfo(storeInfo);

		HashMap<String, Object> result = fileMapper.downloadFile(storeInfo.getUserId(), fileInfo.getFileId());
		Assert.assertEquals(result.get("fileName"), fileInfo.getFileName());
		Assert.assertEquals(result.get("filePath"), storeInfo.getStoreHash());
	}

	@Test
	public void testListFilesInFolder()
	{
		FileInfo fileInfo = newCommonFileInfo();
		StoreInfo storeInfo = newCommonStoreInfo();
		fileInfo.setStoreId(storeInfo.getStoreId());

		fileMapper.insertFileInfo(fileInfo);
		Assert.assertNotEquals(fileMapper.listFilesInFolder(storeInfo.getUserId(), fileInfo.getFolderId()), 0);
	}

}
