/**
 * @FileName TransTaskMapperTest.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/27/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.TransTaskInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @ClassName: TransTaskMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/27/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class TransTaskMapperTest
{
	@Autowired
	private TransTaskMapper transTaskMapper;

	private TransTaskInfo newCommonTransTask()
	{
		String uuid = StringUtil.uuid();
		TransTaskInfo transTaskInfo = new TransTaskInfo(uuid);
		transTaskInfo.setFileHash(uuid);
		transTaskInfo.setStoreId(uuid);
		transTaskInfo.setTaskId(uuid);
		transTaskInfo.setType(1);
		transTaskInfo.setIsDone(0);

		return transTaskInfo;
	}

	@Test
	public void testInsertTransTaskInfo()
	{
		TransTaskInfo transTaskInfo = newCommonTransTask();
		Assert.assertEquals(transTaskMapper.insertTransTaskInfo(transTaskInfo), 1);
	}

	@Test
	public void testDeleteTransTaskInfo()
	{
		TransTaskInfo transTaskInfo = newCommonTransTask();
		Assert.assertEquals(transTaskMapper.insertTransTaskInfo(transTaskInfo), 1);
		Assert.assertEquals(transTaskMapper.deleteTransTaskInfo(transTaskInfo.getTaskId()), 1);
	}

	@Test
	public void testUpdateTransTaskInfo()
	{
		TransTaskInfo transTaskInfo = newCommonTransTask();
		Assert.assertEquals(transTaskMapper.insertTransTaskInfo(transTaskInfo), 1);
		transTaskInfo.setIsDone(1);
		Assert.assertEquals(transTaskMapper.updateTransTaskInfo(transTaskInfo), 1);
	}

	@Test
	public void testGetTransTaskInfo()
	{
		TransTaskInfo transTaskInfo = newCommonTransTask();
		Assert.assertEquals(transTaskMapper.insertTransTaskInfo(transTaskInfo), 1);
		Assert.assertNotNull(transTaskMapper.getTransTaskInfo(transTaskInfo.getTaskId()));
	}

}
