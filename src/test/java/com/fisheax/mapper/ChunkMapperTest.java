/**
 * @FileName ChunkMapperTest.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/27/2016
 */
package com.fisheax.mapper;

import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.ChunkInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @ClassName: ChunkMapperTest
 * @Description
 * @Author fisheax
 * @Date 11/27/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-others.xml")
public class ChunkMapperTest
{
	@Autowired
	private ChunkMapper chunkMapper;

	private ChunkInfo newCommonChunkInfo()
	{
		ChunkInfo chunkInfo = new ChunkInfo(StringUtil.uuid());
		chunkInfo.setTaskId("11111111111111111111111111111111");
		chunkInfo.setStartPos(0);
		chunkInfo.setCurPos(0);
		chunkInfo.setEndPos(0);
		chunkInfo.setIsDone(0);

		return chunkInfo;
	}

	@Test
	public void testInsertChunkInfo()
	{
		ChunkInfo chunkInfo = newCommonChunkInfo();
		Assert.assertEquals(chunkMapper.insertChunkInfo(chunkInfo), 1);
	}

	@Test
	public void testDeleteChunkInfo()
	{
		ChunkInfo chunkInfo = newCommonChunkInfo();
		Assert.assertEquals(chunkMapper.insertChunkInfo(chunkInfo), 1);
		Assert.assertEquals(chunkMapper.deleteChunkInfo(chunkInfo.getChunkId()), 1);
	}

	@Test
	public void testUpdateChunkInfo()
	{
		ChunkInfo chunkInfo = newCommonChunkInfo();
		Assert.assertEquals(chunkMapper.insertChunkInfo(chunkInfo), 1);
		chunkInfo.setIsDone(1);
		chunkInfo.setCurPos(1);
		Assert.assertEquals(chunkMapper.updateChunkInfo(chunkInfo), 1);
	}

	@Test
	public void testGetChunkInfo()
	{
		ChunkInfo chunkInfo = newCommonChunkInfo();
		Assert.assertEquals(chunkMapper.insertChunkInfo(chunkInfo), 1);
		Assert.assertNotNull(chunkMapper.getChunkInfo(chunkInfo.getChunkId()));
	}

	@Test
	public void testGetUnfinishedChunksByTaskId()
	{
		String uuid = StringUtil.uuid();
		ChunkInfo chunkInfo = newCommonChunkInfo();
		ChunkInfo chunkInfo2 = newCommonChunkInfo();
		chunkInfo.setTaskId(uuid);
		chunkInfo2.setTaskId(uuid);
		Assert.assertEquals(chunkMapper.insertChunkInfo(chunkInfo), 1);
		Assert.assertEquals(chunkMapper.insertChunkInfo(chunkInfo2), 1);

		List<ChunkInfo> chunks = chunkMapper.getUnfinishedChunksByTaskId(uuid);
		Assert.assertNotNull(chunks);
		Assert.assertEquals(chunks.size(), 2);
	}
}
