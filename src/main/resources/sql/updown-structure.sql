﻿--
-- Definition for database updown
--
DROP DATABASE IF EXISTS updown;
CREATE DATABASE IF NOT EXISTS updown
	CHARACTER SET utf8
	COLLATE utf8_general_ci;

--
-- Set default database
--
USE updown;

--
-- Definition for table ud_dir_info
--
CREATE TABLE IF NOT EXISTS ud_dir_info (
  uddi_id VARCHAR(32) NOT NULL COMMENT '目录ID',
  uddi_pid VARCHAR(32) NOT NULL COMMENT '父目录ID FK:uddi_id',
  uddi_user_id varchar(32) NOT NULL COMMENT '用户ID FK:udui_id',
  uddi_name VARCHAR(50) NOT NULL COMMENT '目录名称',
  uddi_level INT(11) UNSIGNED NOT NULL COMMENT '目录层级',
  uddi_exists TINYINT(4) NOT NULL COMMENT '是否已删除',
  PRIMARY KEY (uddi_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '虚拟文件目录信息'
ROW_FORMAT = DYNAMIC;

--
-- Definition for table ud_file_info
--
CREATE TABLE IF NOT EXISTS ud_file_info (
  udfi_id VARBINARY(32) NOT NULL COMMENT '虚拟文件信息ID',
  udfi_store_id VARCHAR(32) NOT NULL COMMENT '文件存储信息ID FK:udsi_id',
  udfi_dir_id VARCHAR(32) NOT NULL COMMENT '文件目录信息ID FK:uddi_id',
  udfi_name VARCHAR(50) NOT NULL COMMENT '文件名称',
  udfi_size INT(11) NOT NULL COMMENT '文件大小',
  udfi_up_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文件上传时间',
  udfi_exists TINYINT(4) NOT NULL COMMENT '是否被删除（非物理）',
  PRIMARY KEY (udfi_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '虚拟文件信息'
ROW_FORMAT = DYNAMIC;

--
-- Definition for table ud_logs
--
CREATE TABLE IF NOT EXISTS ud_logs (
  udl_id VARCHAR(32) NOT NULL COMMENT '日志记录ID',
  udl_op_id VARCHAR(32) NOT NULL COMMENT '操作人ID FK:udui_id',
  udl_op_type VARCHAR(50) NOT NULL COMMENT '操作类型',
  udl_op_detail VARCHAR(2000) NOT NULL COMMENT '操作日志明细',
  udl_op_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  udl_op_result tinyint NOT NULL COMMENT '操作结果 0 success -1 failure',
  PRIMARY KEY (udl_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '操作日志记录'
ROW_FORMAT = DYNAMIC;

--
-- Definition for table ud_store_info
--
CREATE TABLE IF NOT EXISTS ud_store_info (
  udsi_id VARCHAR(32) NOT NULL COMMENT '存储文件信息ID',
  udsi_user_id VARCHAR(32) NOT NULL COMMENT '存储文件所属用户ID FK:udui_id',
  udsi_hash VARCHAR(255) NOT NULL COMMENT '存储文件名称/hash',
  udsi_ext_name VARCHAR(255) NOT NULL DEFAULT '' COMMENT '文件扩展名',
  udsi_exists TINYINT(4) NOT NULL COMMENT '存储文件是否被删除',
  udsi_up_done TINYINT(4) NOT NULL DEFAULT 1 COMMENT '文件是否上传完成',
  udsi_last_mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文件最后续传时间',
  PRIMARY KEY (udsi_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '实际文件存储信息'
ROW_FORMAT = DYNAMIC;

--
-- Definition for table ud_trans_task
--
CREATE TABLE IF NOT EXISTS ud_trans_task (
    udtt_id varchar(32) NOT NULL COMMENT '任务ID',
    udtt_store_id varchar(32) NOT NULL COMMENT '存储文件ID FK:udsi_id',
    udtt_file_hash varchar(255) NOT NULL COMMENT '传输文件hash',
    udtt_type tinyint(4) NOT NULL COMMENT '任务传输类型 0: 下载 1: 上传',
    udtt_is_done tinyint(4) NOT NULL COMMENT '任务是否完成 0: 未完成 1: 已完成',
    PRIMARY KEY (udtt_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '分块断点续传任务信息'
ROW_FORMAT = DYNAMIC;

--
-- Definition for table ud_chunk
--
CREATE TABLE IF NOT EXISTS ud_chunk (
    udc_id varchar(32) NOT NULL COMMENT '断点续传信息ID',
    udc_task_id varchar(32) NOT NULL COMMENT '传输任务ID FK:udtt_id',
    udc_start_pos int(11) UNSIGNED NOT NULL COMMENT '区块开始位移',
    udc_cur_pos int(11) UNSIGNED NOT NULL COMMENT '传输结束位移',
    udc_end_pos int(11) UNSIGNED NOT NULL COMMENT '区块末尾位移',
    udc_is_done tinyint(4) NOT NULL COMMENT '区块是否传输完成 0: 未完成 1: 已完成',
    udc_last_mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '区块最后的传输时间',
    PRIMARY KEY (udc_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '断点续传信息'
ROW_FORMAT = DYNAMIC;

--
-- Definition for table ud_user_info
--
CREATE TABLE IF NOT EXISTS ud_user_info (
  udui_id VARCHAR(32) NOT NULL COMMENT '用户信息ID',
  udui_name VARCHAR(255) NOT NULL COMMENT '用户名称',
  udui_pass VARCHAR(255) NOT NULL COMMENT '用户密码',
  udui_salt VARCHAR(8) NOT NULL COMMENT '盐',
  udui_app_secret VARCHAR(255) NOT NULL COMMENT 'App端操作凭证',
  PRIMARY KEY (udui_id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = '用户信息'
ROW_FORMAT = DYNAMIC;