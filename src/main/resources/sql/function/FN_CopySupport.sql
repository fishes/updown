DROP FUNCTION IF EXISTS FN_CopySupport;
CREATE FUNCTION FN_CopySupport(folder_pid VARCHAR(32), folder_id VARCHAR(32))
  RETURNS varchar(4000) CHARSET utf8
  COMMENT '目录递归拷贝辅助函数,将folder_id本身及文件拷贝至folder_pid下,返回folder_id的父+子目录序列对'
BEGIN
    -- 新目录ID
    DECLARE v_new_folder_id varchar(32);
    -- 父目录 level
    DECLARE v_p_level integer;
    -- 返回结果
    DECLARE v_result varchar(4000);

    SET v_new_folder_id = REPLACE(UUID(), '-', '');

    -- 获取父目录的 level
    SELECT
        uddi_level
    INTO
        v_p_level
    FROM ud_dir_info
    WHERE uddi_id = folder_pid
    ;

    -- 拷贝目录本身
    INSERT INTO
        ud_dir_info
    SELECT
        v_new_folder_id
        ,folder_pid
        ,uddi_user_id
        ,uddi_name
        ,v_p_level + 1
        ,1
    FROM ud_dir_info
    WHERE uddi_id = folder_id
        AND uddi_exists = 1
        AND uddi_level != 0
    ;

    -- 拷贝一级目录文件
    INSERT INTO
        ud_file_info
    SELECT
        REPLACE(UUID(), '-', '')
        ,udfi_store_id
        ,v_new_folder_id
        ,udfi_name
        ,udfi_size
        ,udfi_up_time
        ,1
    FROM ud_file_info
    WHERE udfi_dir_id = folder_id
        AND udfi_exists = 1
    ;

    -- 查找一级子目录
    SELECT
        GROUP_CONCAT(CONCAT(v_new_folder_id, ':', uddi_id))
    INTO
        v_result
    FROM ud_dir_info
    WHERE uddi_pid = folder_id
    ;

RETURN v_result;
END