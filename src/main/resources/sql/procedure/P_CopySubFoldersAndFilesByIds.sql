DROP PROCEDURE IF EXISTS P_CopySubFoldersAndFilesByIds;
CREATE PROCEDURE P_CopySubFoldersAndFilesByIds(IN folder_pid VARCHAR(32), IN folder_ids VARCHAR(4000))
  COMMENT '递归拷贝若干指定目录的子目录和文件'
BEGIN
    -- param folder_ids should be like "xxxx,xxxx,xxxx,..."

    -- 文件夹ID
    DECLARE v_folder_id varchar(32);
    -- 文件夹ID总数
    DECLARE v_count integer;
    -- 计数变量
    DECLARE v_index integer;

    SET v_index = 0;
    SET v_count = CHAR_LENGTH(folder_ids) - CHAR_LENGTH(REPLACE(folder_ids, ',', '')) + 1;

    WHILE v_index < v_count DO
        CALL P_CopySubFoldersAndFilesById(folder_pid, SUBSTRING(folder_ids, v_index * 33 + 1, 32));
        SET v_index = v_index + 1;
    END WHILE;
END