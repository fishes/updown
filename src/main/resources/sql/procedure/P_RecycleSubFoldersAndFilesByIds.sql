DROP PROCEDURE IF EXISTS P_RecycleSubFoldersAndFilesByIds;
CREATE PROCEDURE P_RecycleSubFoldersAndFilesByIds(IN folder_ids VARCHAR(2000))
  COMMENT '递归软删除指定若干目录的子目录及文件'
BEGIN
    -- param folder_ids should be like "xxxx,xxxx,xxxx,..."

    -- 文件夹ID
    DECLARE v_folder_id varchar(32);
    -- 文件夹ID总数
    DECLARE v_count integer;
    -- 计数变量
    DECLARE v_index integer;

    SET v_index = 0;
    SET v_count = CHAR_LENGTH(folder_ids) - CHAR_LENGTH(REPLACE(folder_ids, ',', '')) + 1;

    WHILE v_index < v_count DO
        CALL P_RecycleSubFoldersAndFilesById(SUBSTRING(folder_ids, v_index * 33 + 1, 32));
        SET v_index = v_index + 1;
    END WHILE;

END