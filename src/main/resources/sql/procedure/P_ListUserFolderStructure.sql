DROP PROCEDURE IF EXISTS P_ListUserFolderStructure;
CREATE PROCEDURE P_ListUserFolderStructure(IN user_id VARCHAR(32), IN x_folder_ids VARCHAR(1000), OUT tree_json VARCHAR(8000))
  COMMENT '生成指定用户的目录结构,返回指定格式的JSON串,x_folder_ids 是要排除的目录ID列表'
BEGIN
    -- 用户根目录ID
    DECLARE v_root_folder_id varchar(32);

    -- 获取用户根目录ID
    SELECT
        uddi_id
    INTO
        v_root_folder_id
    FROM ud_dir_info
    WHERE uddi_user_id = user_id
        AND uddi_name = '/'
        AND uddi_level = 0
        AND uddi_exists = 1
    ;

    CALL P_ListUserFolderStructureSupport(v_root_folder_id, x_folder_ids, tree_json);
    SET tree_json = CONCAT('[', tree_json, ']');
END