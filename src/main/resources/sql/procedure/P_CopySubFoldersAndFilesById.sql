DROP PROCEDURE IF EXISTS P_CopySubFoldersAndFilesById;
CREATE PROCEDURE P_CopySubFoldersAndFilesById(IN folder_pid VARCHAR(32), IN folder_id VARCHAR(32))
  COMMENT '递归拷贝某个指定目录的子目录和文件'
BEGIN
    -- 下一级的目录ID
    DECLARE v_folder_ids varchar(4000);
    -- 临时存储
    DECLARE v_t_folder_ids varchar(4000);
    -- 临时存储
    DECLARE v_t_ids varchar(4000);
    -- 父子目录ID对
    DECLARE v_pair varchar(65);
    -- 父子目录ID总数
    DECLARE v_count integer;
    -- 计数变量
    DECLARE v_index integer;

    START TRANSACTION;

        SET v_folder_ids = 'NOT_NULL';
        SET v_t_folder_ids = FN_CopySupport(folder_pid, folder_id);

        -- 开始遍历
        WHILE v_folder_ids IS NOT NULL DO
            SET v_folder_ids = '';
            SET v_index = 0;
            SET v_count = CHAR_LENGTH(v_t_folder_ids) - CHAR_LENGTH(REPLACE(v_t_folder_ids, ',', '')) + 1;

            WHILE v_index < v_count DO
                SET v_pair = SUBSTRING(v_t_folder_ids, v_index * 66 + 1, 65);
                SET v_t_ids = FN_CopySupport(SUBSTRING(v_pair, 1, 32), SUBSTRING(v_pair, -32, 32));

                IF CHAR_LENGTH(v_folder_ids) = 0 THEN
                    SET v_folder_ids = v_t_ids;
                ELSEIF v_t_ids IS NOT NULL THEN
                    SET v_folder_ids = CONCAT(v_folder_ids, ',', v_t_ids);
                END IF;

                SET v_index = v_index + 1;
            END WHILE;

            SET v_t_folder_ids = v_folder_ids;
        END WHILE;

    COMMIT;
END