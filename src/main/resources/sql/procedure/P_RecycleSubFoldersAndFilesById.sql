DROP PROCEDURE IF EXISTS P_RecycleSubFoldersAndFilesById;
CREATE PROCEDURE P_RecycleSubFoldersAndFilesById(IN folder_id VARCHAR(32))
  COMMENT '递归软删除指定单个目录的子目录及文件'
BEGIN

    -- 同层级的目录ID
    DECLARE v_folder_ids varchar(2000);
    DECLARE v_folder_exists integer;

    DROP TABLE IF EXISTS t_ids;
    CREATE TEMPORARY TABLE t_ids (
        tid varchar(32)
    );

    START TRANSACTION;
        -- 软删除文件夹本身
        UPDATE ud_dir_info uddi
        SET uddi_exists = 0
        WHERE uddi.uddi_id = folder_id
        -- 再次过滤根目录
        AND uddi.uddi_level != 0
        ;

        -- 软删除本目录文件
        INSERT INTO t_ids
            SELECT
                udfi_id
            FROM ud_file_info
            WHERE udfi_dir_id = folder_id
        ;

        UPDATE ud_file_info
        SET udfi_exists = 0
        WHERE udfi_id IN (SELECT
                tid
            FROM t_ids);

        DELETE
            FROM t_ids;

        -- 先判定存在子目录, 然后再初始化
        SELECT
            COUNT(1) INTO v_folder_exists
        FROM ud_dir_info udi
        WHERE udi.uddi_pid = folder_id
        ;

        IF v_folder_exists != 0 THEN
            -- 初始化目录ID序列
            SELECT
                GROUP_CONCAT(uddi.uddi_id) INTO v_folder_ids
            FROM ud_dir_info uddi
            WHERE uddi.uddi_pid = folder_id
            ;
        END IF;

        -- 开始遍历
        WHILE v_folder_ids IS NOT NULL DO
            -- 软删除文件夹
            UPDATE ud_dir_info uddi
            SET uddi_exists = 0
            WHERE FIND_IN_SET(uddi.uddi_id, v_folder_ids)
            ;

            -- 软删除文件
            INSERT INTO t_ids
                SELECT
                    udfi_id
                FROM ud_file_info
                WHERE FIND_IN_SET(udfi_dir_id, v_folder_ids)
            ;

            UPDATE ud_file_info
            SET udfi_exists = 0
            WHERE udfi_id IN (SELECT
                    tid
                FROM t_ids);

            DELETE
                FROM t_ids;

            -- 寻找下一级目录
            SELECT
                COUNT(1) INTO v_folder_exists
            FROM ud_dir_info uddi
            WHERE FIND_IN_SET(uddi.uddi_pid, v_folder_ids)
            ;

            IF v_folder_exists != 0 THEN
                SELECT
                    GROUP_CONCAT(uddi.uddi_id) INTO v_folder_ids
                FROM ud_dir_info uddi
                WHERE FIND_IN_SET(uddi.uddi_pid, v_folder_ids)
                ;
            ELSE
                SET v_folder_ids = NULL;
            END IF;
        END WHILE;

    COMMIT;
END