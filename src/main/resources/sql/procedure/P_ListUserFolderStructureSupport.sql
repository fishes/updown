DROP PROCEDURE IF EXISTS P_ListUserFolderStructureSupport;
CREATE PROCEDURE P_ListUserFolderStructureSupport(IN folder_id VARCHAR(32), IN x_folder_ids VARCHAR(1000), OUT result VARCHAR(8000))
  COMMENT '文件目录树生成辅助过程,返回目录树结构JSON串. x_folder_ids 是要过滤掉的目录ID'
xstart:
BEGIN
    -- 常量声明
    DECLARE c_id_left varchar(32);
    DECLARE c_id_title varchar(32);
    DECLARE c_title_nodes varchar(32);
    DECLARE c_nodes_right varchar(32);

    -- 目录名称
    DECLARE v_folder_name varchar(255);

    -- 子目录ID总数
    DECLARE v_count integer;
    -- 计数变量
    DECLARE v_index integer;
    -- 当前遍历子目录ID
    DECLARE v_sub_folder_id varchar(32);
    -- 子目录ID序列
    DECLARE v_sub_folder_ids varchar(1000);

    -- 临时结果存储
    DECLARE v_t_result varchar(8000);
    -- 临时结果存储
    DECLARE v_pt_result varchar(8000);

    -- 是否处理本目录 (是否在排除列表里)
    DECLARE v_if_continue integer;

    SELECT
        1
    INTO
        v_if_continue
    FROM dual
    WHERE FIND_IN_SET(folder_id, x_folder_ids)
    ;

    IF v_if_continue = 1 THEN
        -- 如果在排除的列表ID里, 就不处理本目录
        SET result = '';
        LEAVE xstart;
    END IF;

    --
    SET max_sp_recursion_depth=255;

    -- 常量赋值
    SET c_id_left = '{"id":"';
    SET c_id_title = '","title":"';
    SET c_title_nodes = '","nodes":[';
    SET c_nodes_right = ']}';

    -- 寻找下级目录
    SELECT
        GROUP_CONCAT(uddi_id)
    INTO
        v_sub_folder_ids
    FROM ud_dir_info
    WHERE uddi_pid = folder_id
        AND uddi_exists = 1
    ;

    IF v_sub_folder_ids IS NULL THEN
        -- 最后一级目录处理
        SELECT
            CONCAT(c_id_left, uddi_id, c_id_title, uddi_name, c_title_nodes, c_nodes_right)
        INTO
            result
        FROM ud_dir_info
        WHERE uddi_id = folder_id
        ;
    ELSE
        -- 当前目录名称
        SELECT
            uddi_name
        INTO
            v_folder_name
        FROM ud_dir_info
        WHERE uddi_id = folder_id
        ;

        SET v_t_result = CONCAT(c_id_left, folder_id, c_id_title, v_folder_name, c_title_nodes);
        SET v_count = CHAR_LENGTH(v_sub_folder_ids) - CHAR_LENGTH(REPLACE(v_sub_folder_ids, ',', '')) + 1;
        SET v_index = 0;

        WHILE v_index < v_count DO
            SET v_sub_folder_id = SUBSTRING(v_sub_folder_ids, v_index * 33 + 1, 32);
            CALL P_ListUserFolderStructureSupport(v_sub_folder_id, x_folder_ids, v_pt_result);

            IF v_pt_result != '' THEN
                SET v_t_result = CONCAT(v_t_result, v_pt_result, ',');
            END IF;

            SET v_index = v_index + 1;
        END WHILE;

        IF SUBSTRING(v_t_result, -1, 1) = '[' THEN
            -- 没有符合的子目录
            SET result = CONCAT(v_t_result, c_nodes_right);
        ELSE
            SET result = CONCAT(SUBSTRING(v_t_result, 1, CHAR_LENGTH(v_t_result) - 1), c_nodes_right);
        END IF;
    END IF;
END xstart