/**
 * 消息显示服务
 */
main.service("msgSrv", function () {
    // 要显示的消息内容
    this.message = "";
});

/**
 * 数据共享服务
 */
main.service("shareSrv", function () {
    // 用户ID
    this.userId = "";
    // 根目录ID
    this.rootFolderId = "";
    // 当前目录ID
    this.curFolderId = "";
    // 已选择的目录或文件ID
    this.selected = [];
    // 选中的文件/文件夹名称
    this.selectedName = "";

    // 目录树数据
    this.treeData = [];
});

/**
 * 获取 MD5 Hash 服务
 */
main.service("hashSrv", function () {
    // 获取文件 MD5 Hash
    this.hashFile = function (file, info, refresh) {
        let blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
            chunkSize = 2097152,                             // Read in chunks of 2MB
            chunks = Math.ceil(file.size / chunkSize),
            currentChunk = 0,
            spark = new SparkMD5.ArrayBuffer(),
            fileReader = new FileReader();

        fileReader.onload = function (e) {
            spark.append(e.target.result);                   // Append array buffer
            currentChunk++;

            if (currentChunk < chunks) {
                loadNext();
            } else {
                info.hash = spark.end();  // Compute hash
                refresh();
                console.log(info.hash);
            }
        };

        fileReader.onerror = function () {
            console.warn('oops, something went wrong.');
        };

        function loadNext() {
            let start = currentChunk * chunkSize,
                end = ((start + chunkSize) >= file.size) ? file.size : start + chunkSize;

            fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
        }

        loadNext();
    };

    // 获取 Blob MD5 Hash
    this.hashBlob = function (chunk, info) {
        let spark = new SparkMD5.ArrayBuffer();
        spark.append(chunk);
        info.hash = spark.end();
    };
});