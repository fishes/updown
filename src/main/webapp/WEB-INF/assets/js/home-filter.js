/**
 * 文件大小显示过滤器
 */
main.filter("filesize", function () {
    return function (size) {
        let fileSize;
        if (size > 1024 * 1024)
            fileSize = (Math.round(size * 100 / (1024 * 1024)) / 100).toString() + "M";
        else
            fileSize = (Math.round(size * 100 / 1024) / 100).toString() + "K";

        return fileSize;
    };
});