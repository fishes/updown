let main = angular.module("updown", ["ui.bootstrap", "ui.tree"]);

/**
 * angular http post converter
 */
main.config(function ($httpProvider) {
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        /**
         * The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        let param = function (obj) {
            let query = '';
            let name, value, fullSubName, subName, subValue, innerObj, i;

            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value !== undefined && value !== null) {
                    query += encodeURIComponent(name) + '='
                        + encodeURIComponent(value) + '&';
                }
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };

        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

/**
 * 页面主要逻辑控制器
 */
main.controller("homeCtrl", ["$scope", "$http", "$uibModal", "msgSrv", "shareSrv",
    function ($scope, $http, $uibModal, msgSrv, shareSrv) {
        $scope.userId = "";
        $scope.rootFolderId = "";
        $scope.curFolderId = "";
        $scope.selected = [];
        $scope.folders = [];
        $scope.foldersNum = 0;
        $scope.files = [];
        $scope.filesNum = 0;
        $scope.pathes = [];

        // 全选
        $scope.selectAll = function () {
            let checkFlag = $("#select-all").prop("checked");
            angular.forEach($("#folders").add($("#files")).find("input:checkbox"), function (cbox) {
                let boxes = $(cbox);
                boxes.prop("checked", checkFlag);
                if (checkFlag) {
                    $scope.selected.push(boxes.val());
                    shareSrv.selected = $scope.selected;
                }
            });

            if (!checkFlag) {
                $scope.selected = [];
                shareSrv.selected = [];
            }
        };

        // 单选
        $scope.selectOne = function (event) {
            let target = event.target;
            if (target.checked) {
                $scope.selected.push(target.value);
                if ($scope.selected.length === $scope.foldersNum + $scope.filesNum) {
                    $("#select-all").prop("checked", true);
                }
            } else {
                $scope.selected.remove(target.value);
                $("#select-all").prop("checked", false);
            }
            shareSrv.selected = $scope.selected;
        };

        // 页面数据初始化
        $scope.init = function () {
            $scope.userId = $("#userId").text();
            shareSrv.userId = $scope.userId;
            $http.post(web_context_path + "/folder/root", {"userId": $scope.userId})
                .then(function (resp) {
                    if (resp.data.code === 0) {
                        $scope.rootFolderId = resp.data.data.folderId;
                        shareSrv.rootFolderId = $scope.rootFolderId;
                        $scope.pathes.push({"folderId": $scope.rootFolderId, "folderName": "/"});
                        $scope.curFolderId = $scope.rootFolderId;
                        shareSrv.curFolderId = $scope.curFolderId;
                        $scope.load($scope.rootFolderId);
                    }
                }, function (resp) {
                    $scope.showMsg(resp.data.message);
                });
        };

        // 加载指定ID下的文件夹及文件
        $scope.load = function (folderId) {
            $scope.selected = [];
            $http.post(web_context_path + "/folder/list", {"folderId": folderId})
                .then(function (resp) {
                    if (resp.data.code === 0) {
                        $scope.folders = resp.data.data.folders;
                        $scope.foldersNum = $scope.folders.length;
                        $scope.files = resp.data.data.files;
                        $scope.filesNum = $scope.files.length;

                        angular.forEach($scope.files, function (file) {
                            file.uploadTime = new Date(file.uploadTime).format("yyyy-MM-dd hh:mm:ss");
                        });
                    }
                }, function (resp) {
                    $scope.showMsg(resp.data.message);
                });
        };

        $scope.refresh = function () {
            // 在没有文件/文件夹的情况下, 选中 select-all 后进行操作, 表现与全选不符
            $("#select-all").prop("checked", false);
            $scope.load(shareSrv.curFolderId);
        };

        $scope.$on("refresh-file-list", function () {
            $scope.refresh();
        });

        // 显示错误消息
        $scope.showMsg = function (msg) {
            msgSrv.message = msg;
            $uibModal.open({
                templateUrl: "msg-prompt.html",
                controller: "msgCtrl"
            });
        };

        // 文件浏览
        $scope.browser = function (event) {
            $scope.curFolderId = $(event.target).parent().prev().val().split(":")[1];
            shareSrv.curFolderId = $scope.curFolderId;
            $scope.pathes.push({
                "folderId": $scope.curFolderId,
                "folderName": $(event.target).text()
            });
            $scope.refresh();
        };

        // 目录导航
        $scope.navigate = function (folderId) {
            $scope.curFolderId = folderId;
            shareSrv.curFolderId = $scope.curFolderId;
            let newPathes = [];

            for (let i = 0; i < $scope.pathes.length; i++) {
                let path = $scope.pathes[i];
                newPathes.push(path);

                if (path.folderId === folderId) {
                    break;
                }
            }

            $scope.pathes = newPathes;
            $scope.refresh();
        };

        // 文件选择
        $scope.selectFile = function () {
            let form = $("<form>");
            form.attr("style", "display:none");
            form.attr("method", "post");
            form.attr("enctype", "multipart/form-data");

            let file = $("<input>");
            file.attr("type", "file");
            file.attr("name", "files");
            file.on("change", function () {
                $scope.$broadcast("upfile-selected", form);
            });
            form.append(file);

            $("body").append(form);
            file.click();
        };

        // 文件夹创建
        $scope.newFolder = function () {
            $uibModal.open({
                templateUrl: "new-folder-name.html",
                controller: "nameCtrl"
            });
        };

        // 文件下载
        $scope.download = function () {
            let length = $scope.selected.length;

            if (length === 0) {
                $scope.showMsg("Please choose a file");
            } else if (length !== 1) {
                $scope.showMsg("Do not support serveral files at once");
            } else if ($scope.selected[0].indexOf("file:") === -1) {
                $scope.showMsg("From now only support download a file");
            } else {
                let form = $("<form>");
                form.attr("style", "display:none");
                form.attr("target", "");
                form.attr("method", "post");
                form.attr("action", web_context_path + "/download/" + $scope.selected[0].split(":")[1]);

                $("body").append(form);
                form.submit();
            }
        };

        // 文件或文件夹重命名
        $scope.rename = function () {
            let length = $scope.selected.length;

            if (length === 0) {
                $scope.showMsg("Please choose a file");
            } else if (length !== 1) {
                $scope.showMsg("Do not support serveral files at once");
            } else {
                shareSrv.selectedName = $("#folders").add($("#files")).find("input:checked").next().next().text();
                $uibModal.open({
                    templateUrl: "rename.html",
                    controller: "renameCtrl"
                });
            }
        };

        // 文件或文件夹拷贝
        $scope.copy = function () {
            showFolderTree("treeCopyCtrl");
        };

        // 文件或文件夹移动
        $scope.move = function () {
            showFolderTree("treeMoveCtrl");
        };

        let showFolderTree = function (controller) {
            if ($scope.selected.length === 0) {
                $scope.showMsg("Please choose at least one folder or file");
            } else if ($("#select-all").prop("checked") && $scope.curFolderId === $scope.rootFolderId) {
                $scope.showMsg("No more place for " + (controller === "treeCopyCtrl" ? "copying" : "moving"));
            } else {
                let xfolderIds = [];

                if ($("#select-all").prop("checked")) {
                    xfolderIds.push($scope.curFolderId);
                } else {
                    shareSrv.selected.forEach(function (id) {
                        if (id.indexOf("folder:") !== -1) {
                            xfolderIds.push(id.split(":")[1]);
                        }
                    });

                    if (xfolderIds.length === $scope.foldersNum) {
                        // 目录全选就替换为当前目录ID
                        xfolderIds = [$scope.curFolderId];
                    }
                }

                $.ajax({
                    type: "post",
                    url: web_context_path + "/folder/tree",
                    dataType: "json",
                    data: {
                        "xfolderIds": xfolderIds
                    }
                }).done(function (resp) {
                    if (resp.code === 0) {
                        shareSrv.treeData = JSON.parse(resp.data);
                        $uibModal.open({
                            templateUrl: "tree.html",
                            controller: controller
                        });
                    } else {
                        $scope.showMsg(resp.data.message);
                    }
                });
            }
        };

        // 文件或文件夹删除
        $scope.delete = function () {
            let length = $scope.selected.length;

            if (length === 0) {
                $scope.showMsg("Please choose a file");
            } else {
                $uibModal.open({
                    templateUrl: "delete.html",
                    controller: "deleteCtrl"
                });
            }
        };
    }]);

/**
 * 队列上传逻辑控制
 */
main.controller("uqCtrl", ["$scope", "hashSrv", "$http",
    function ($scope, hashSrv, $http) {
        $scope.upfilesInfo = new Map();
        $scope.upfiles = [];
        $scope.sendQueue = new Map();

        $scope.update = function () {
            $scope.upfiles = [];
            $scope.upfilesInfo.forEach(function (value) {
                $scope.upfiles.push(value[0]);
            });
        };

        // 接收显示用户选择上传的文件
        $scope.$on("upfile-selected", function (event, form) {
            let upfile = form.children()[0].files[0];
            let file = {};

            file.fileId = Math.floor(Math.random() * 10000000) + "";
            file.fileName = upfile.name;
            file.folderId = $scope.curFolderId;
            file.fullPath = $scope.pathes.map(function (obj) {
                    return obj.folderName;
                }).join("/").substring(1) + "/";
            file.fileSize = upfile.size;
            file.lastModified = upfile.lastModified;
            // -1 - prepare ; 0 - init ; 1 - uploading ; 2 - pause; 3 - stopped ; 4 - done
            file.status = "prepare";
            file.uploaded = 0;
            file.progress = {width : 0};

            // hash file may cost a little more time, may do not use file hash in the future
            // hashSrv.hashFile(upfile, file, function () {
            //     // refresh, I do not know why $scope.$watch did not work properly
            //     $scope.upfilesInfo.get(file.fileId)[0].status = "init";
            //     $scope.update();
            //     $scope.$apply();
            // });
            // alternative operation for caculating file's md5 hash
            file.hash = "fake_hash_to_reduce_time_" + file.fileId;
            file.status = "init";

            $scope.upfilesInfo.set(file.fileId, [file, form]);
            $scope.update();
            $scope.$apply();
        });

        // 删除之前选中的文件
        $scope.delete = function (fileId) {
            if (!$scope.upfilesInfo.delete(fileId)) {
                $scope.showMsg("Can not find the file");
            } else {
                $scope.update();
            }
        };

        // 暂停文件上传
        $scope.pause = function (fileId) {
            $scope.sendQueue.get(fileId).paused = true;
            $scope.upfilesInfo.get(fileId)[0].status = "pause";
            $scope.update();
        };

        // 取消文件上传
        $scope.stop = function (fileId) {
            // pause
            $scope.sendQueue.get(fileId).paused = true;
            let tmpUpfileInfo = $scope.upfilesInfo.get(fileId)[0];
            tmpUpfileInfo.status = "stopped";
            $scope.update();

            // notice the server
            $http.post(web_context_path + "/task/stop", {
                "taskId" : localStorage.getItem(tmpUpfileInfo.hash)
            });

            // remove the task id at local storage
            localStorage.removeItem(tmpUpfileInfo.hash);
        };

        // 开始文件上传
        $scope.upload = function (fileId) {
            // check if started the upload queue
            let tmpQueue = $scope.sendQueue.get(fileId);
            let tmpUpfileInfo = $scope.upfilesInfo.get(fileId);
            if (tmpQueue) {
                tmpUpfileInfo[0].status = "uploading";
                $scope.update();
                tmpQueue.paused = false;
                return;
            }

            let upfileInfo = tmpUpfileInfo[0];
            upfileInfo.status = "uploading";
            let upfile = tmpUpfileInfo[1].children()[0].files[0];
            // try to get task id
            let taskId = localStorage.getItem(upfileInfo.hash);
            let taskInfo = {};
            let uploadQueue;
            let send = function () {
                // analyse chunk info and then build upload queue
                uploadQueue = new UploadQueue(fileId, upfile, taskInfo);
                $scope.sendQueue.set(fileId, uploadQueue);
                uploadQueue.init();

                // pump the chunks
                uploadQueue.pump(function (data, taskId, queue) {
                    let info = data[0];
                    let formData = new FormData();
                    formData.append("taskId", taskId);
                    formData.append("chunkId", info.chunkId);
                    formData.append("startPos", info.startPos);
                    formData.append("size", info.chunkSize);
                    formData.append("hash", info.hash);
                    formData.append("chunk", new File([data[1]], "fakename"));

                    $.ajax({
                        url: web_context_path + "/upload",
                        type: "POST",
                        cache: false,
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (resp) {
                        // acknowledgement whether success or not
                        queue.producing--;

                        resp = JSON.parse(resp);
                        if (resp.code == -512) {
                            // cope with the bad hash
                            // reprepare the chunk ...
                            console.error("The chunk received is with bad hash");
                        } else if (resp.code != 0) {
                            queue.uploadQueue.push(data);
                        } else {
                            // progress bar is simply coped, calculate after every post
                            upfileInfo.uploaded += info.chunkSize;
                            // 0.99 is for the server merging chunks operation, it cost a little time
                            upfileInfo.progress.width =
                                Math.round(upfileInfo.uploaded * 0.99 / upfileInfo.fileSize * 100) + "%";
                            $scope.update();
                            $scope.$apply();
                        }
                    }).fail(function () {
                        // acknowledgement whether success or not
                        queue.producing--;

                        // push back the bad request
                        queue.uploadQueue.push(data);
                        console.log("upload the chunk failed, would try again");
                    });
                }, $scope);
            };

            if (!taskId) {
                // create new task
                $http.post(web_context_path + "/task/new", {
                    "folderId" : upfileInfo.folderId,
                    "fileName" : upfileInfo.fileName,
                    "fileSize" : upfileInfo.fileSize,
                    "fileHash" : upfileInfo.hash
                }).then(function (resp) {
                        if (resp.data.code === 0) {
                            taskInfo = resp.data.data;
                            // obtain taskId for next transport from break
                            localStorage.setItem(upfileInfo.hash, taskInfo.taskId);
                            send();
                        } else {
                            $scope.showMsg(resp.data.message);
                        }
                    }, function (resp) {
                        $scope.showMsg(resp.data.message);
                    });
            } else {
                // query transport chunks info
                $http.post(web_context_path + "/task/info", {
                    "taskId" : taskId
                }).then(function (resp) {
                    if (resp.data.code === 0) {
                        taskInfo = resp.data.data;
                        send();
                    } else {
                        localStorage.removeItem(taskId);
                        $scope.showMsg(resp.data.message);
                    }
                }, function (resp) {
                    // localStorage.removeItem(that.taskId);  // do not do it here
                    $scope.showMsg(resp.data.message);
                });
            }
        };

        function UploadQueue(fileId, file, taskInfo) {
            this.fileId = fileId;
            this.file = file;
            this.chunksInfo = taskInfo.chunks;

            this.uploadQueue = [];
            this.chunkSize = 2097152;  // 2M
            this.chunksPos = [];

            // if waiting for retriving the piece of file
            // may casue the problem while rounding the "code block"
            this.producing = 0;
            // if produced and consumed all the chunks
            this.paused = false;

            // init
            this.init = function () {
                // analyze untransported chunks info
                let that = this;
                this.chunksInfo.forEach(function (chunkInfo) {
                    let size = chunkInfo.endPos - chunkInfo.startPos + 1;
                    let count = Math.floor(size / that.chunkSize);

                    for (let index = 0; index < count; index++) {
                        that.chunksPos.push({
                            chunkId : chunkInfo.chunkId,
                            startPos : chunkInfo.startPos + index * that.chunkSize,
                            chunkSize : that.chunkSize
                        });
                    }

                    let lastChunkSize = size % that.chunkSize;
                    if (lastChunkSize !== 0)
                        that.chunksPos.push({
                            chunkId : chunkInfo.chunkId,
                            startPos : chunkInfo.startPos + count * that.chunkSize,
                            chunkSize : lastChunkSize
                        });
                });

                console.log("Chunks number : " + that.chunksPos.length);
                // angular.forEach(that.chunksPos, function (pos) {
                //     console.log("Position : " + pos.startPos + "-" + (pos.startPos + pos.chunkSize))
                // });

                // fill the upload queue with 5 chunks by default
                let count = Math.min(that.chunksPos.length, 5);
                for (let index = 0; index < count; index++)
                    that.fetch(that.chunksPos.shift(), that.uploadQueue);
            };

            // fetch chunk from file
            this.fetch = function (chunkInfo, queue) {
                let fileReader = new FileReader();
                let that = this;

                fileReader.onload = function (e) {
                    hashSrv.hashBlob(e.target.result, chunkInfo);
                    queue.push([chunkInfo, e.target.result]);
                    that.producing--;
                };

                fileReader.onerror = function () {
                    console.warn("Oops, something went wrong. Reserve the chunk info");
                    that.chunksPos.push(chunkInfo);
                };

                that.producing++;
                fileReader.readAsArrayBuffer(file.slice(chunkInfo.startPos, chunkInfo.startPos + chunkInfo.chunkSize));
            };

            // pump chunk from upload queue
            this.pump = function (sendChunk, scope) {
                let that = this;
                let handler = {};

                handler.h = setInterval(function () {
                    if (that.paused)
                        return;

                    let chunkPos = undefined;
                    let data = undefined;
                    if (data = that.uploadQueue.shift()) {
                        // if can get data, then return
                        (chunkPos = that.chunksPos.shift()) && that.fetch(chunkPos, that.uploadQueue);
                        that.producing++;
                        sendChunk(data, taskInfo.taskId, that);
                    } else if (!(chunkPos = that.chunksPos[0]) && that.producing == 0) {
                        let tfile = scope.upfilesInfo.get(that.fileId)[0];
                        tfile.status = "show-nothing-while-merge-chunks";
                        scope.update();

                        // if no one producing and no more chunk info, call server then done
                        $http.post(web_context_path + "/task/done", {
                            "taskId" : taskInfo.taskId
                        }).then(function (resp) {
                            if (resp.data.code != 0) {
                                // how to do while can not complete the upload task ???
                                scope.showMsg("Error while complete the upload task" + resp.data.message);
                            } else {
                                tfile.status = "done";
                                tfile.uploaded = tfile.fileSize;
                                tfile.progress.width = "100%";
                                scope.update();
                                scope.$emit("refresh-file-list");
                            }
                            console.log(scope.upfilesInfo.get(that.fileId)[0].hash);
                            localStorage.removeItem(scope.upfilesInfo.get(that.fileId)[0].hash);
                        }, function () {
                            scope.showMsg("Error while complete the upload task");
                            localStorage.removeItem(scope.upfilesInfo.get(that.fileId)[0].hash);
                        });

                        clearInterval(handler.h);
                    } else {
                        // if in other suitations, do nothing
                    }
                }, 100);
            };
        }
    }]);

/**
 * 错误消息显示控制器
 */
main.controller("msgCtrl", ["$scope", "$uibModalInstance", "msgSrv",
    function ($scope, $uibModalInstance, msgSrv) {
        $scope.msg = msgSrv.message;

        $scope.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };
    }]);

/**
 * 文件夹创建控制器
 */
main.controller("nameCtrl", ["$scope", "$uibModalInstance", "$uibModal", "msgSrv", "shareSrv", "$http",
    function ($scope, $uibModalInstance, $uibModal, msgSrv, shareSrv, $http) {
        $scope.newFolderName = "";

        $scope.ok = function () {
            if ($scope.newFolderName === "") {
                $uibModalInstance.dismiss("cancel");
                $scope.showMsg("The folder name can not be empty");
                return;
            }

            $http.post(web_context_path + "/folder/create", {
                "folderPid": shareSrv.curFolderId,
                "folderName": $scope.newFolderName
            }).then(function (resp) {
                if (resp.data.code === 0) {
                    $("#home").scope().refresh();
                } else {
                    $scope.showMsg(resp.data.message);
                }
            }, function (resp) {
                $scope.showMsg(resp.data.message);
            });

            $uibModalInstance.close("ok");
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };

        $scope.showMsg = function (msg) {
            msgSrv.message = msg;
            $uibModal.open({
                templateUrl: "msg-prompt.html",
                controller: "msgCtrl"
            });
        };
    }]);

/**
 * 文件/文件夹重命名控制器
 */
main.controller("renameCtrl", ["$scope", "$uibModalInstance", "$uibModal", "msgSrv", "shareSrv", "$http",
    function ($scope, $uibModalInstance, $uibModal, msgSrv, shareSrv, $http) {
        let originalName = shareSrv.selectedName;
        let selectedId = shareSrv.selected[0];
        $scope.newName = shareSrv.selectedName;

        $scope.ok = function () {
            if ($scope.newName === "") {
                $uibModalInstance.dismiss("cancel");
                $scope.showMsg("The new name can not be empty");
                return;
            } else if (originalName !== $scope.newName) {
                if (selectedId.indexOf("file:") !== -1) {
                    // rename file
                    $http.post(web_context_path + "/file/rename", {
                        "fileId": selectedId.split(":")[1],
                        "newFileName": $scope.newName
                    }).then(function (resp) {
                        if (resp.data.code === 0) {
                            $("#home").scope().refresh();
                        } else {
                            $scope.showMsg(resp.data.message);
                        }
                    }, function (resp) {
                        $scope.showMsg(resp.data.message);
                    });
                } else {
                    // rename folder
                    $http.post(web_context_path + "/folder/rename", {
                        "folderId": selectedId.split(":")[1],
                        "newFolderName": $scope.newName
                    }).then(function (resp) {
                        if (resp.data.code === 0) {
                            $("#home").scope().refresh();
                        } else {
                            $scope.showMsg(resp.data.message);
                        }
                    }, function (resp) {
                        $scope.showMsg(resp.data.message);
                    });
                }
            }

            $uibModalInstance.close("ok");
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };

        $scope.showMsg = function (msg) {
            msgSrv.message = msg;
            $uibModal.open({
                templateUrl: "msg-prompt.html",
                controller: "msgCtrl"
            });
        };
    }]);

/**
 * 文件/文件夹重命名控制器
 */
main.controller("deleteCtrl", ["$scope", "$uibModalInstance", "$uibModal", "msgSrv", "shareSrv",
    function ($scope, $uibModalInstance, $uibModal, msgSrv, shareSrv) {
        let folderIds = [];
        let fileIds = [];

        $scope.ok = function () {
            shareSrv.selected.forEach(function (id) {
                if (id.indexOf("file:") !== -1) {
                    fileIds.push(id.split(":")[1]);
                } else {
                    folderIds.push(id.split(":")[1]);
                }
            });

            // $http 的数据格式在上面被处理过, 所以这里使用 jQuery 来发包
            if (folderIds.length !== 0) {
                // delete folders
                $.ajax({
                    type: "post",
                    url: web_context_path + "/folder/delete",
                    dataType: "json",
                    data: {
                        "folderIds": folderIds
                    }
                }).done(function () {
                    $("#home").scope().refresh();
                });
            }

            if (fileIds.length !== 0) {
                // delete files
                $.ajax({
                    type: "post",
                    url: web_context_path + "/file/delete",
                    dataType: "json",
                    data: {
                        "fileIds": fileIds
                    }
                }).done(function () {
                    $("#home").scope().refresh();
                });
            }

            $uibModalInstance.close("ok");
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };

        $scope.showMsg = function (msg) {
            msgSrv.message = msg;
            $uibModal.open({
                templateUrl: "msg-prompt.html",
                controller: "msgCtrl"
            });
        };
    }]);

/**
 * 文件/文件夹拷贝控制器
 */
main.controller("treeCopyCtrl", ["$scope", "$uibModalInstance", "$uibModal", "msgSrv", "shareSrv",
    function ($scope, $uibModalInstance, $uibModal, msgSrv, shareSrv) {
        $scope.selectedId = "";
        $scope.data = shareSrv.treeData;
        let folderIds = [];
        let fileIds = [];

        $scope.check = function (subject) {
            $scope.selectedId = subject.$element.children().last().text();
        };

        $scope.ok = function () {
            if ($scope.selectedId === "") {
                $uibModalInstance.close("ok");
                $scope.showMsg("Please choose the target folder");
                return;
            } else if ($scope.selectedId === shareSrv.curFolderId) {
                $uibModalInstance.close("ok");
                $scope.showMsg("I don't know why can't copy to the current folder, BUT I just made it");
                return;
            }

            shareSrv.selected.forEach(function (id) {
                if (id.indexOf("file:") !== -1) {
                    fileIds.push(id.split(":")[1]);
                } else {
                    folderIds.push(id.split(":")[1]);
                }
            });

            // $http 的数据格式在上面被处理过, 所以这里使用 jQuery 来发包
            if (folderIds.length !== 0) {
                // move folders
                $.ajax({
                    type: "post",
                    url: web_context_path + "/folder/copy",
                    dataType: "json",
                    data: {
                        "destFolderId": $scope.selectedId,
                        "folderIds": folderIds
                    }
                }).done(function () {
                    $("#home").scope().refresh();
                });
            }

            if (fileIds.length !== 0) {
                // move files
                $.ajax({
                    type: "post",
                    url: web_context_path + "/file/copy",
                    dataType: "json",
                    data: {
                        "folderId": $scope.selectedId,
                        "fileIds": fileIds
                    }
                }).done(function () {
                    $("#home").scope().refresh();
                });
            }

            $uibModalInstance.close("ok");
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };

        $scope.showMsg = function (msg) {
            msgSrv.message = msg;
            $uibModal.open({
                templateUrl: "msg-prompt.html",
                controller: "msgCtrl"
            });
        };
    }]);

/**
 * 文件/文件夹移动控制器
 */
main.controller("treeMoveCtrl", ["$scope", "$uibModalInstance", "$uibModal", "msgSrv", "shareSrv",
    function ($scope, $uibModalInstance, $uibModal, msgSrv, shareSrv) {
        $scope.selectedId = "";
        $scope.data = shareSrv.treeData;
        let folderIds = [];
        let fileIds = [];

        $scope.check = function (subject) {
            $scope.selectedId = subject.$element.children().last().text();
            console.log("checked " + $scope.selectedId);
        };

        $scope.ok = function () {
            if ($scope.selectedId === "") {
                $uibModalInstance.close("ok");
                $scope.showMsg("Please choose the target folder");
                return;
            } else if ($scope.selectedId === shareSrv.curFolderId) {
                $uibModalInstance.close("ok");
                $scope.showMsg("I don't know why can't move to the current folder, BUT I just made it");
                return;
            }

            shareSrv.selected.forEach(function (id) {
                if (id.indexOf("file:") !== -1) {
                    fileIds.push(id.split(":")[1]);
                } else {
                    folderIds.push(id.split(":")[1]);
                }
            });

            // $http 的数据格式在上面被处理过, 所以这里使用 jQuery 来发包
            if (folderIds.length !== 0) {
                // move folders
                $.ajax({
                    type: "post",
                    url: web_context_path + "/folder/move",
                    dataType: "json",
                    data: {
                        "destFolderId": $scope.selectedId,
                        "folderIds": folderIds
                    }
                }).done(function () {
                    $("#home").scope().refresh();
                });
            }

            if (fileIds.length !== 0) {
                // move files
                $.ajax({
                    type: "post",
                    url: web_context_path + "/file/move",
                    dataType: "json",
                    data: {
                        "folderId": $scope.selectedId,
                        "fileIds": fileIds
                    }
                }).done(function () {
                    $("#home").scope().refresh();
                });
            }

            $uibModalInstance.close("ok");
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };

        $scope.showMsg = function (msg) {
            msgSrv.message = msg;
            $uibModal.open({
                templateUrl: "msg-prompt.html",
                controller: "msgCtrl"
            });
        };
    }]);