<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	String context = request.getContextPath();
%>
<html data-ng-app="updown">
<head>
	<title>Home</title>
	<link rel="icon" href="<%=context%>/favicon.ico" type="image/x-icon"/>
	<link rel="stylesheet" href="<%=context%>/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="<%=context%>/angular-ui-tree/angular-ui-tree.css">
	<link rel="stylesheet" href="<%=context%>/css/home.css">
	<script src="<%=context%>/jquery/jquery.js"></script>
	<script src="<%=context%>/angular/angular.js"></script>
	<script src="<%=context%>/angular-ui/ui-bootstrap-tpls.js"></script>
	<script src="<%=context%>/angular-ui-tree/angular-ui-tree.js"></script>
	<script src="<%=context%>/js/date-util.js"></script>
	<script src="<%=context%>/js/array-util.js"></script>
	<script src="<%=context%>/js/spark-md5.js"></script>
	<script src="<%=context%>/js/home-ctrl.js"></script>
	<script src="<%=context%>/js/home-srv.js"></script>
	<script src="<%=context%>/js/home-filter.js"></script>
	<script>
		window.web_context_path = <%= "".equals(context) ? "\"\"" : "\"" + context + "\"" %>;
	</script>
</head>
<body>
<div class="space"></div>
<div id="home" class="panel panel-primary container" data-ng-controller="homeCtrl" data-ng-init="init()">
	<div class="panel-title row">
		<span class="h3 col-md-12 w-title">Welcome to home, ${sessionScope.user.username} !!!</span>
		<span id="userId" style="display: none;">${sessionScope.user.userId}</span>
	</div>
	<div class="panel-body">
		<div id="opbar" class="row form-group">
			<button id="btnSelect" class="col-md-1 btn btn-default form-inline" data-ng-click="selectFile()">Upload</button>
			<button id="btnNew" class="col-md-1 btn btn-default form-inline" data-ng-click="newFolder()">New Folder</button>
			<button id="btnDown" class="col-md-1 btn btn-default form-inline" data-ng-show="selected.length"
					data-ng-click="download()">Download</button>
			<button id="btnRen" class="col-md-1 btn btn-default form-inline" data-ng-click="rename()"
					data-ng-show="selected.length">Rename</button>
			<button id="btnDel" class="col-md-1 btn btn-default form-inline" data-ng-click="delete()"
					data-ng-show="selected.length">Delete</button>
			<button id="btnCp" class="col-md-1 btn btn-default form-inline" data-ng-click="copy()"
					data-ng-show="selected.length">Copy To</button>
			<button id="btnMv" class="col-md-1 btn btn-default form-inline" data-ng-click="move()"
					data-ng-show="selected.length">Move To</button>
			<a href="<%=context%>/user/logout" id="btnOut" class="col-md-1 btn btn-default pull-right">LogOut</a>
		</div>
		<div id="upQueue" data-ng-controller="uqCtrl" data-ng-show="upfilesInfo.size !== 0">
			<div id="upqTitle" class="row">
				<span>UPLOAD QUEUE : </span>
			</div>
			<div id="upfiles">
				<div class="row" data-ng-repeat="upfile in upfiles track by $index">
					<div class="col-md-4">
						<span class="glyphicon glyphicon-file"></span>
						<span data-ng-bind="upfile.fileName" class="upfile-name"></span>
					</div>
					<div class="col-md-4">
						<span data-ng-bind="upfile.fullPath === '' ? '/' : upfile.fullPath"></span>
					</div>
					<div class="col-md-1" data-ng-bind="upfile.fileSize | filesize"></div>
					<div id="uploadOps" class="col-md-1">
						<span class="glyphicon glyphicon glyphicon-remove" data-ng-click="delete(upfile.fileId)"
							  data-ng-show="upfile.status === 'prepare' || upfile.status === 'init'"></span>
						<span class="glyphicon glyphicon-play" data-ng-click="upload(upfile.fileId)"
							  data-ng-show="upfile.status === 'init' || upfile.status === 'pause'"></span>
						<span class="glyphicon glyphicon-pause" data-ng-click="pause(upfile.fileId)"
							  data-ng-show="upfile.status === 'uploading'"></span>
						<span class="glyphicon glyphicon-stop" data-ng-click="stop(upfile.fileId)"
							  data-ng-show="upfile.status === 'uploading' || upfile.status === 'pause'"></span>
						<span class="glyphicon glyphicon-ok" data-ng-show="upfile.status === 'done'"></span>
						<span class="glyphicon glyphicon-ban-circle" data-ng-show="upfile.status === 'stopped'"></span>
					</div>
					<div id="upProgress" class="col-md-2 progress">
						<div id="progressBar" class="progress-bar" data-ng-bind="upfile.progress.width"
							 data-ng-style="upfile.progress"></div>
					</div>
				</div>
			</div>
		</div>
		<div id="navigator" class="row">
			<span>CURRENT PLACE : </span>
			<span data-ng-repeat="path in pathes" data-ng-bind="path.folderName + ($index + 1 != pathes.length ? ' > ' : '')"
				  data-ng-class="$index + 1 == pathes.length ? 'nav-no-click' : 'nav-sep'"
				  data-ng-click="navigate(path.folderId)">
			</span>
		</div>
		<div id="list">
			<div id="title" class="row">
				<div id="title-name" class="col-md-8">
					<input type="checkbox" id="select-all" data-ng-click="selectAll()"
						   class="form-inline box">FILENAME
				</div>
				<div id="title-size" class="col-md-1">SIZE</div>
				<div id="title-time" class="col-md-3">TIME</div>
			</div>
			<div id="folders">
				<div class="row show-line" data-ng-repeat="folder in folders">
					<div class="col-md-8">
						<input type="checkbox" class="form-inline box" value="{{'folder:' + folder.folderId}}"
							   data-ng-click="selectOne($event)">
						<div class="folder-name" style="display: inline-block">
							<span class="glyphicon glyphicon-folder-open"></span>
							<span data-ng-bind="folder.folderName" data-ng-click="browser($event)"></span>
						</div>
					</div>
					<div class="col-md-1">-</div>
					<div class="col-md-3">-</div>
				</div>
			</div>
			<div id="files">
				<div class="row show-line" data-ng-repeat="file in files">
					<div class="col-md-8">
						<input type="checkbox" class="form-inline box" value="{{'file:' + file.fileId}}"
							   data-ng-click="selectOne($event)">
						<span class="glyphicon glyphicon-file"></span>
						<span data-ng-bind="file.fileName"></span>
					</div>
					<div class="col-md-1" data-ng-bind="file.fileSize | filesize"></div>
					<div class="col-md-3" data-ng-bind="file.uploadTime"></div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/ng-template" id="msg-prompt.html">
		<div class="modal-header">
			<h3 class="modal-title">MESSAGE INFO</h3>
		</div>
		<div class="modal-body">
			Message : <span data-ng-bind="msg"></span>
		</div>
		<div class="modal-footer">
			<button class="btn btn-default" data-ng-click="cancel()">GOT IT</button>
		</div>
	</script>
	<script type="text/ng-template" id="new-folder-name.html">
		<div class="modal-header">
			<h4 class="modal-title">PLEASE INPUT THE FOLDER NAME</h4>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-md-4 control-label" style="font-size: larger;">New Folder Name : </label>
					<div class="col-md-8" style="padding-left: 0;">
						<input type="text" class="form-control" name="folderName" data-ng-model="newFolderName" required>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button class="btn btn-default" data-ng-click="ok()">OK</button>
			<button class="btn btn-default" data-ng-click="cancel()">CANCEL</button>
		</div>
	</script>
	<script type="text/ng-template" id="rename.html">
		<div class="modal-header">
			<h4 class="modal-title">PLEASE INPUT THE NEW NAME</h4>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-md-4 control-label" style="font-size: larger;">New Name : </label>
					<div class="col-md-8" style="padding-left: 0;">
						<input type="text" class="form-control" name="newName" data-ng-model="newName" required>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button class="btn btn-default" data-ng-click="ok()">OK</button>
			<button class="btn btn-default" data-ng-click="cancel()">CANCEL</button>
		</div>
	</script>
	<script type="text/ng-template" id="delete.html">
		<div class="modal-header">
			<h4 class="modal-title">WARNING !!!</h4>
		</div>
		<div class="modal-body">
			<span class="h4">ARE YOU SURE TO DELETE THEM ?</span>
		</div>
		<div class="modal-footer">
			<button class="btn btn-default" data-ng-click="ok()">OK</button>
			<button class="btn btn-default" data-ng-click="cancel()">CANCEL</button>
		</div>
	</script>
	<script type="text/ng-template" id="tree.html">
		<div class="modal-header">
			<h4 class="modal-title">PLEASE CHOOSE THE TARGET FOLDER</h4>
		</div>
		<div class="modal-body">
			<div class="h4" style="margin-bottom: 10px;">The folders tree :</div>
			<div data-ui-tree id="tree-root" style="height: 200px; overflow: scroll; margin-left: 20px;">
				<ol data-ui-tree-nodes data-ng-model="data">
					<li data-ng-repeat="node in data track by $index" data-ui-tree-node data-ng-include="'tree-nodes.html'"></li>
				</ol>
			</div>
		</div>
		<div class="modal-footer">
			<button class="btn btn-default" data-ng-click="ok()">OK</button>
			<button class="btn btn-default" data-ng-click="cancel()">CANCEL</button>
		</div>
	</script>
	<script type="text/ng-template" id="tree-nodes.html">
		<div ui-tree-handle class="tree-node tree-node-content">
			<div data-nodrag data-ng-click="check(this)" data-ng-class="{'node-selected' : node.id == selectedId }">
				<span class="glyphicon glyphicon-folder-open"></span>
				<span data-ng-bind="' ' + node.title" style="margin-left: 3px;"></span>
			</div>
			<div style="display: none;">{{node.id}}</div>
		</div>
		<ol data-ui-tree-nodes="" data-ng-model="node.nodes">
			<li data-ng-repeat="node in node.nodes track by $index" data-ui-tree-node data-ng-include="'tree-nodes.html'">
			</li>
		</ol>
	</script>
</div>
</body>
</html>
