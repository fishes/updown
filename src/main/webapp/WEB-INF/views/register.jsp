<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	String context = request.getContextPath();
%>
<html>
<head>
	<title>Register</title>
	<link rel="icon" href="<%=context%>/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="<%=context%>/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="<%=context%>/css/register.css">
</head>
<body>
<div class="space"></div>
<div class="panel panel-primary container">
	<div class="panel-title row">
		<span class="h2 col-md-12 w-title">Welcome to register !!!</span>
	</div>
	<div class="panel-title row">
		<span class="h4 col-md-12 w-title err">${msg}</span>
	</div>
	<div class="panel-body">
		<form action="<%=context%>/user/register/request" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-md-offset-1 col-md-3">USERNAME:</label>
				<div class="col-md-5" id="username">
					<input type="text" name="username" class="form-control" placeholder="enter your username" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-offset-1 col-md-3">PASSWORD:</label>
				<div class="col-md-5" id="password">
					<input type="password" name="password" class="form-control" placeholder="enter your password" required>
				</div>
			</div>
			<div class="form-group">
				<button id="regBtn" type="submit" class="btn btn-default col-md-offset-4 col-md-1">REGISTER</button>
				<button type="reset" class="btn btn-default col-md-1">RESET</button>
				<a href="<%=context%>/user/login" id="loginBtn" class="btn btn-default col-md-1">LOGIN</a>
			</div>
		</form>
	</div>
</div>
<%
	String msg = ((String) request.getAttribute("msg"));
	if (msg != null && !"".equals(msg) && !"register failed".equals(msg)) {
%>
<script>
	window.setTimeout(function () {
		window.location = "<%=context%>/user/login";
	}, 1000);
</script>
<%
	}
%>
</body>
</html>
