/**
 * @FileName AuthController.java
 * @Package com.fisheax.restapi;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.restapi;

import com.fisheax.component.ResultWrapper;
import com.fisheax.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @ClassName: AuthController
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@Controller
@RequestMapping("/rest")
public class AuthController
{
	private UserMapper userMapper;

	@ResponseBody
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public ResultWrapper authKey(String secretKey, HttpSession session, Errors errors)
	{
		// 这里的验证放在 session 里似乎不太妥当, 再考虑下吧
		return null;
	}

	// ================================================================================================

	@Autowired
	public AuthController(UserMapper userMapper)
	{
		Assert.notNull(userMapper);
		this.userMapper = userMapper;
	}
}
