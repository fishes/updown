/**
 * @FileName ResultWrapper.java
 * @Package com.fisheax.component;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.component;

import java.util.Collection;
import java.util.Map;

/**
 * @ClassName: ResultWrapper
 * @Description 结果包装器
 * @Author fisheax
 * @Date 11/7/2016
 */
public class ResultWrapper
{
	private int code;
	private String message;
	private Object data;

	public static String successInfo = "success";
	public static String failedInfo = "unkown error";

	// wrappers

	public static ResultWrapper success()
	{
		return success(successInfo);
	}

	public static ResultWrapper success(String message)
	{
		return wrap(0, message);
	}

	public static ResultWrapper failed()
	{
		return failed(failedInfo);
	}

	public static ResultWrapper failed(String message)
	{
		return wrap(-1024, message);
	}

	public static ResultWrapper auto(boolean condition)
	{
		return auto(condition, successInfo, failedInfo);
	}

	public static ResultWrapper auto(boolean condition, String successMsg, String failedMsg)
	{
		return condition ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper autoByNull(Object object)
	{
		return autoByNull(object, successInfo, failedInfo);
	}

	public static ResultWrapper autoByNull(Object object, String successMsg, String failedMsg)
	{
		return object == null ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper autoByNotNull(Object object)
	{
		return autoByNotNull(object, successInfo, failedInfo);
	}

	public static ResultWrapper autoByNotNull(Object object, String successMsg, String failedMsg)
	{
		return object != null ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper autoByEmpty(Object object)
	{
		return autoByEmpty(object, successInfo, failedInfo);
	}

	public static ResultWrapper autoByEmpty(Object object, String successMsg, String failedMsg)
	{
		return isEmpty(object) ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper autoByNotEmpty(Object object)
	{
		return autoByNotEmpty(object, successInfo, failedInfo);
	}

	public static ResultWrapper autoByNotEmpty(Object object, String successMsg, String failedMsg)
	{
		return !isEmpty(object) ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper autoByEquals(Object object, Object expected)
	{
		return autoByEquals(object, expected, successInfo, failedInfo);
	}

	public static ResultWrapper autoByEquals(Object object, Object expected, String successMsg, String failedMsg)
	{
		return object != null && object.equals(expected) ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper autoByNotEquals(Object object, Object expected)
	{
		return autoByNotEquals(object, expected, successInfo, failedInfo);
	}

	public static ResultWrapper autoByNotEquals(Object object, Object expected, String successMsg, String failedMsg)
	{
		return object == null || !object.equals(expected) ? success(successMsg) : failed(failedMsg);
	}

	public static ResultWrapper wrap(int code)
	{
		return new ResultWrapper(code, "", null);
	}

	public static ResultWrapper wrap(int code, String message)
	{
		return new ResultWrapper(code, message, null);
	}

	public static ResultWrapper wrap(int code, String message, Object data)
	{
		return new ResultWrapper(code, message, data);
	}

	// ================================================================================================

	// checkers

	public boolean succeed()
	{
		return code == 0;
	}

	public boolean fail()
	{
		return code != 0;
	}

	// ================================================================================================

	// callbacks to set data

	public ResultWrapper succeed(Object data)
	{
		if (code == 0)
			this.data = data;

		return this;
	}

	public ResultWrapper fail(Object data)
	{
		if (code != 0)
			this.data = data;

		return this;
	}

	// ================================================================================================

	// 判断对象是否"为空"
	private static boolean isEmpty(Object object)
	{
		if (object == null)
			return false;

		if (object.getClass().isArray())
			return ((Object[]) object).length == 0;
		else if (object instanceof Collection)
			return ((Collection) object).isEmpty();
		else if (object instanceof Map)
			return ((Map) object).isEmpty();

		return false;
	}

	private ResultWrapper(int code, String message, Object data)
	{
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int code()
	{
		return code;
	}

	public ResultWrapper code(int code)
	{
		this.code = code;
		return this;
	}

	public String message()
	{
		return message;
	}

	public ResultWrapper message(String message)
	{
		this.message = message;
		return this;
	}

	public Object data()
	{
		return data;
	}

	public ResultWrapper data(Object data)
	{
		this.data = data;
		return this;
	}

	// ================================================================================================


	@Override
	public String toString()
	{
		return "ResultWrapper{" +
			"code=" + code +
			", message='" + message + '\'' +
			", data=" + data +
			'}';
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public Object getData()
	{
		return data;
	}

	public void setData(Object data)
	{
		this.data = data;
	}
}
