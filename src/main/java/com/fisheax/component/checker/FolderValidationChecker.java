/**
 * @FileName FolderValidationChecker.java
 * @Package com.fisheax.component.checker;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.component.checker;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: FolderValidationChecker
 * @Description 文件夹名称检查
 * @Author fisheax
 * @Date 11/4/2016
 */
public interface FolderValidationChecker
{
	/**
	 * 检查文件夹名称是否符合要求
	 * @param folderName
	 * @return
	 */
	boolean validateFolderName(String folderName);

	/**
	 * 过滤掉根目录ID
	 * @param rootFolderId
	 * @param folderIds
	 * @return
	 */
	static String[] filterRootFolderId(String rootFolderId, String[] folderIds)
	{
		if (folderIds == null || rootFolderId == null)
			return new String[0];

		List<String> strings = Arrays.asList(folderIds);
		strings.remove(rootFolderId);

		return ((String[]) strings.toArray());
	}
}
