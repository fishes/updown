/**
 * @FileName FileValidationChecker.java
 * @Package com.fisheax.component;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.component.checker;

/**
 * @ClassName: FileValidationChecker
 * @Description 文件合法性检查
 * @Author fisheax
 * @Date 11/4/2016
 */
public interface FileValidationChecker
{
	/**
	 * 检查文件名称是否符合要求
	 * @param fileName
	 * @return
	 */
	boolean validateFileName(String fileName);

	/**
	 * 检查文件扩展名是否符合要求
	 * @param fileExtName
	 * @return
	 */
	boolean validateFileExtName(String fileExtName);

	/**
	 * 检查文件内容是否符合要求
	 * @param fileContent
	 * @return
	 */
	boolean validateFileContent(byte[] fileContent);
}
