/**
 * @FileName DefaultFolderValidationChecker.java
 * @Package com.fisheax.component.checker.defaults;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.component.checker.defaults;

import com.fisheax.component.checker.FolderValidationChecker;
import org.springframework.stereotype.Component;

/**
 * @ClassName: DefaultFolderValidationChecker
 * @Description
 * @Author fisheax
 * @Date 11/4/2016
 */
@Component
public class DefaultFolderValidationChecker implements FolderValidationChecker
{
	/**
	 * 默认不检查文件夹名称
	 * @param folderName
	 * @return
	 */
	@Override
	public boolean validateFolderName(String folderName)
	{
		return !"/".equals(folderName);
	}
}
