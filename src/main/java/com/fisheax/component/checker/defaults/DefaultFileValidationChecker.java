/**
 * @FileName DefaultFileValidationChecker.java
 * @Package com.fisheax.component.checker;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.component.checker.defaults;

import com.fisheax.component.checker.FileValidationChecker;
import org.springframework.stereotype.Component;

/**
 * @ClassName: DefaultFileValidationChecker
 * @Description
 * @Author fisheax
 * @Date 11/4/2016
 */
@Component
public class DefaultFileValidationChecker implements FileValidationChecker
{

	/**
	 * 默认不检查文件名称
	 * @param fileName
	 * @return
	 */
	@Override
	public boolean validateFileName(String fileName)
	{
		return true;
	}

	/**
	 * 默认只允许扩展名为 txt 的文件
	 * @param fileExtName
	 * @return
	 */
	@Override
	public boolean validateFileExtName(String fileExtName)
	{
		return true;
	}

	/**
	 * 默认不检查文件内容
	 * @param fileContent
	 * @return
	 */
	@Override
	public boolean validateFileContent(byte[] fileContent)
	{
		return true;
	}
}
