/**
 * @FileName EncyptUtil.java
 * @Package com.fisheax.component.utils;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.component.utils;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @ClassName: EncyptUtil
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
public class EncyptUtil
{
	/**
	 * 取字符串 (UTF-8 编码) 的 MD5 值 (UTF-8 编码)
	 * @param source
	 * @return
	 */
	public static String md5(String source)
	{
		if (source == null)
			return null;

		byte[] result = md5(source.getBytes(Charset.forName("UTF-8")));
		return result == null ? null : new String(Base64.getEncoder().encode(result), Charset.forName("UTF-8"));
	}

	/**
	 * 取 byte[] 的 MD5 值
	 * @param bytes
	 * @return
	 */
	public static byte[] md5(byte[] bytes)
	{
		if (bytes == null)
			return null;

		try
		{
			return MessageDigest.getInstance("MD5").digest(bytes);
		}
		catch (NoSuchAlgorithmException ex) { return null; }
	}

	/**
	 * 取 byte[] 的 MD5 值 (UTF-8 编码)
	 * @param bytes
	 * @return
	 */
	public static String md5str(byte[] bytes)
	{
		byte[] result = md5(bytes);
		if (result == null)
			return null;

		StringBuilder builder = new StringBuilder();
		for (byte b : result)
			builder.append(Integer.toHexString((0x000000ff & b) | 0xffffff00).substring(6));

		return builder.toString();
	}
}
