/**
 * @FileName LogUtil.java
 * @Package com.fisheax.component.utils;
 * @Author fisheax
 * @Date 11/14/2016
 */
package com.fisheax.component.utils;

import com.alibaba.fastjson.JSON;
import com.fisheax.pojo.*;

import java.util.HashMap;

/**
 * @ClassName: LogUtil
 * @Description
 * @Author fisheax
 * @Date 11/14/2016
 */
public class LogUtil
{
	/**
	 * 生成日志信息
	 * @param type
	 * @param subject
	 * @return
	 */
	public static LogInfo get(String opId, OpType type, boolean succeed, String detail, Object subject)
	{
		LogInfo logInfo = newCommonLogInfo(opId);
		String opType = type == null ? "UNKOWN" : type.toString();
		String opDetail = detail == null ? opType + " : " + createDetail(subject) : detail;
		logInfo.setOpType(opType);
		logInfo.setOpDetail(opDetail);
		logInfo.setOpResult(succeed ? 0 : -1);
		return logInfo;
	}

	/**
	 * json 化需要传入的参数, 用于日志记录
	 * @param objects
	 * @return
	 */
	public static String strinifyWithName(boolean withName, Object ... objects)
	{
		if (withName)
		{
			if (objects == null || objects.length % 2 == 1)
				return "";

			HashMap<String, Object> params = new HashMap<>();
			for (int index = 0; index < objects.length; index += 2)
				params.put(((String) objects[index]), objects[index+1]);

			return JSON.toJSONString(params);
		}
		else
			return JSON.toJSONString(objects);
	}

	// ================================================================================================

	private static LogInfo newCommonLogInfo(String opId)
	{
		LogInfo logInfo = new LogInfo(StringUtil.uuid());
		logInfo.setOpId(opId);
		return logInfo;
	}

	// 简单处理, 直接 toString
	private static String createDetail(Object subject)
	{
		if (subject instanceof UserInfo ||
			subject instanceof FolderInfo ||
			subject instanceof FileInfo ||
			subject instanceof StoreInfo)
			return subject.toString();

		return "";
	}

	// ================================================================================================

	/**
	 * 操作类型
	 */
	public enum OpType
	{
		CREATE, DELETE, UPDATE, QUERY, UNKOWN
	}
}
