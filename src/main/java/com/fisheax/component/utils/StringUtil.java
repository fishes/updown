/**
 * @FileName StringUtil.java
 * @Package com.fisheax.component.utils;
 * @Author fisheax
 * @Date 11/6/2016
 */
package com.fisheax.component.utils;

import java.util.UUID;

/**
 * @ClassName: StringUtil
 * @Description
 * @Author fisheax
 * @Date 11/6/2016
 */
public class StringUtil
{
	/**
	 * 判断字符串是否为空
	 * @param target
	 * @return
	 */
	public static boolean empty(String target)
	{
		return target == null || "".equals(target);
	}

	/**
	 * 生成 32 位 uuid
	 * @return
	 */
	public static String uuid()
	{
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * 去除字符串头尾一个字符
	 * @param source
	 * @return
	 */
	public static String removeHeadAndTail(String source)
	{
		return removeHeadAndTail(source, 1, 1);
	}

	/**
	 * 去除字符串头尾指定个字符
	 * @param source
	 * @param headCount
	 * @param tailCount
	 * @return
	 */
	public static String removeHeadAndTail(String source, int headCount, int tailCount)
	{
		int length = 0;
		if (headCount < 0 || tailCount < 0)
			throw new IllegalArgumentException("Invalid head or tail count number, postive is allowed");
		if (source == null || (length = source.length()) <= headCount + tailCount)
			return "";

		return source.substring(headCount, length - tailCount);
	}

	/**
	 * 使用 ',' join 一个字符串数组,
	 * @param subjects
	 * @return
	 */
	public static String join(String[] subjects)
	{
		return join(subjects, null);
	}

	/**
	 * 使用 separator (默认 ',') join 一个字符串数组,
	 * @param subjects
	 * @param separator
	 * @return
	 */
	public static String join(String[] subjects, String separator)
	{
		if (subjects == null)
			return "";

		if (separator == null)
			separator = ",";

		StringBuilder builder = new StringBuilder();
		for (int index = 0; index < subjects.length - 1; index++)
			builder.append(subjects[index]).append(separator);

		return builder.append(subjects[subjects.length - 1]).toString();
	}

}
