/**
 * @FileName DoNotLogThisMethod.java
 * @Package com.fisheax.component;
 * @Author fisheax
 * @Date 11/14/2016
 */
package com.fisheax.component.annotations;

import java.lang.annotation.*;

/**
 * @ClassName: DoNotLogThisMethod
 * @Description 标注方法不要进行日志记录
 * @Author fisheax
 * @Date 11/14/2016
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DoNotLogThisMethod
{
}
