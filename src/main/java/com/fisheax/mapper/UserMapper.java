/**
 * @FileName FileMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.UserInfo;

/**
 * @ClassName: FileMapper
 * @Description 文件操作 DAO
 * @Author fisheax
 * @Date 11/4/2016
 */
public interface UserMapper
{
	/**
	 * 增加一个用户信息
	 * @param info
	 * @return
	 */
	int insertUserInfo(UserInfo info);

	/**
	 * 删除一个用户信息
	 * @param userId
	 * @return
	 */
	int deleteUserInfo(String userId);

	/**
	 * 更新一个用户信息
	 * @param info
	 * @return
	 */
	int updateUserInfo(UserInfo info);

	/**
	 * 通过用户ID获取一个用户信息
	 * @param userId
	 * @return
	 */
	UserInfo getUserInfoById(String userId);

	/**
	 * 通过用户名得到用户信息
	 * @param username
	 * @return
	 */
	UserInfo getUserInfoByName(String username);

	/**
	 * 检测用户名是否被占用
	 * @param username
	 * @return
	 */
	boolean isUsernameOccupied(String username);

	/**
	 * 检测用户是否存在
	 * @param info
	 * @return
	 */
	boolean isUserValid(UserInfo info);

	/**
	 * 检测是否 app key 是否存在
	 * @param secretKey
	 * @return
	 */
	boolean isKeyValid(String secretKey);
}
