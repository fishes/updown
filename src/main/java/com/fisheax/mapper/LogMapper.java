/**
 * @FileName LogMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/6/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.LogInfo;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

/**
 * @ClassName: LogMapper
 * @Description
 * @Author fisheax
 * @Date 11/6/2016
 */
public interface LogMapper
{
	/**
	 * 插入一条 log
	 * @param info
	 * @return
	 */
	int insertLogInfo(LogInfo info);

	/**
	 * 删除一条 log
	 * @param logId
	 * @return
	 */
	int deleteLogInfo(String logId);

	/**
	 * 更新一条 log
	 * @param info
	 * @return
	 */
	int updateLogInfo(LogInfo info);

	/**
	 * 获取一条 log
	 * @param logId
	 * @return
	 */
	LogInfo getLog(String logId);

	/**
	 * 获取所有的 log
	 * @return
	 */
	List<LogInfo> getAllLogs();

	/**
	 * 获取指定时间段的 log
	 * @param timeFrom
	 * @param timeTo
	 * @return
	 */
	List<LogInfo> getLogsByTime(@Param("timeFrom") Timestamp timeFrom,
	                            @Param("timeTo") Timestamp timeTo);
}
