/**
 * @FileName TransTaskMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/21/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.StoreInfo;
import com.fisheax.pojo.TransTaskInfo;

/**
 * @ClassName: TransTaskMapper
 * @Description
 * @Author fisheax
 * @Date 11/21/2016
 */
public interface TransTaskMapper
{
	/**
	 * 增加一个任务信息
	 * @param transTaskInfo
	 * @return
	 */
	int insertTransTaskInfo(TransTaskInfo transTaskInfo);

	/**
	 * 物理软删除一个任务信息
	 * @param taskId
	 * @return
	 */
	int deleteTransTaskInfo(String taskId);

	/**
	 * 更新一个任务信息
	 * @param transTaskInfo
	 * @return
	 */
	int updateTransTaskInfo(TransTaskInfo transTaskInfo);

	/**
	 * 标注任务完成
	 * @param taskId
	 * @return
	 */
	int finishTransTask(String taskId);

	/**
	 * 获取一个任务信息
	 * @param taskId
	 * @return
	 */
	TransTaskInfo getTransTaskInfo(String taskId);

	/**
	 * 获取 task 对于的 store 信息
	 * @param taskId
	 * @return
	 */
	StoreInfo getTaskStoreInfo(String taskId);

	/**
	 * 获取任务对应文件大小
	 * @param taskId
	 * @return
	 */
	long getFileSizeByTaskId(String taskId);
}
