/**
 * @FileName FolderMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.FileInfo;
import com.fisheax.pojo.FolderInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: FolderMapper
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
public interface FolderMapper
{
	/**
	 * 新增一个虚拟目录信息
	 * @param info
	 * @return
	 */
	int insertFolderInfo(FolderInfo info);

	/**
	 * 创建用户虚拟根目录信息
	 * @param userId
	 * @return
	 */
	int createRootFolder(String userId);

	/**
	 * 拷贝虚拟目录
	 * @param params map 中需要 folderIds, folderPid
	 * @return
	 */
	int copyFolders(Map<String, Object> params);

	/**
	 * 物理删除一个虚拟目录信息
	 * @param folderId
	 * @return
	 */
	int deleteFolderInfoPermanently(String folderId);

	/**
	 * 软删除一个虚拟目录信息
	 * @param folderId
	 * @return
	 */
	int deleteFolderInfo(@Param("folderId") String folderId);

	/**
	 * 软删除N个虚拟目录(这里没有支持递归删除, 对普通用户而言是看不到子目录了, 但是其确实还存在)
	 * @param folderIds
	 * @return
	 */
	int deleteFolders(@Param("folderIds") String folderIds);

	/**
	 * 更新一个虚拟目录信息
	 * @param info
	 * @return
	 */
	int updateFolderInfo(FolderInfo info);

	/**
	 * 移动虚拟目录
	 * @param folderIds
	 * @return
	 */
	int moveFolders(@Param("folderIds") String[] folderIds,
	                @Param("folderPid") String folderPid);

	/**
	 * 获取一个虚拟目录信息
	 * @param folderId
	 * @return
	 */
	FolderInfo getFolderInfo(String folderId);

	/**
	 * 获取用户虚拟根目录信息
	 * @param userId
	 * @return
	 */
	FolderInfo getRootFolderInfo(String userId);

	/**
	 * 获取一个虚拟目录下一级子目录信息
	 * @param folderPid
	 * @return
	 */
	List<FolderInfo> getSubFoldersById(String folderPid);

	/**
	 * 获取指定目录下的所有虚拟文件信息
	 * @param folderId
	 * @return
	 */
	List<FileInfo> getFilesInDir(String folderId);

	/**
	 * 获取某个用户的文件夹结构
	 * @param params map 中需要有 userId
	 * @return
	 */
	String getFolderStruture(Map<String, Object> params);
}
