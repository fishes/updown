/**
 * @FileName ChunkMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/21/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.ChunkInfo;

import java.util.List;

/**
 * @ClassName: ChunkMapper
 * @Description
 * @Author fisheax
 * @Date 11/21/2016
 */
public interface ChunkMapper
{
	/**
	 * 插入一个分片信息
	 * @param chunkInfo
	 * @return
	 */
	int insertChunkInfo(ChunkInfo chunkInfo);

	/**
	 * 插入多个分片信息
	 * @param chunkInfos
	 * @return
	 */
	int insertChunkInfos(List<ChunkInfo> chunkInfos);

	/**
	 * 物理删除一个分片信息
	 * @param chunkId
	 * @return
	 */
	int deleteChunkInfo(String chunkId);

	/**
	 * 物理删除一个任务相关联的所有分片信息
	 * @param taskId
	 * @return
	 */
	int deleteChunkInfosByTaskId(String taskId);

	/**
	 * 更新一个分片信息
	 * @param chunkInfo
	 * @return
	 */
	int updateChunkInfo(ChunkInfo chunkInfo);

	/**
	 * 查询一个分片信息
	 * @param chunkId
	 * @return
	 */
	ChunkInfo getChunkInfo(String chunkId);

	/**
	 * 查询一个任务未完成传输的分片信息
	 * @param taskId
	 * @return
	 */
	List<ChunkInfo> getUnfinishedChunksByTaskId(String taskId);
}
