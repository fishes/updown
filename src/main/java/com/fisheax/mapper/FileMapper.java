/**
 * @FileName FileMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.FileInfo;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName: FileMapper
 * @Description 文件操作 DAO
 * @Author fisheax
 * @Date 11/4/2016
 */
public interface FileMapper
{
	/**
	 * 增加一个虚拟文件信息
	 * @param info
	 * @return
	 */
	int insertFileInfo(FileInfo info);

	/**
	 * 拷贝虚拟文件到指定目录
	 * @param fileIds
	 * @return
	 */
	int copyFiles(@Param("fileIds") String[] fileIds,
	              @Param("folderId") String folderId);

	/**
	 * 物理删除某个文件信息
	 * @param fileId
	 * @return
	 */
	int deleteFileInfoPermanently(String fileId);

	/**
	 * 软删除某个文件信息
	 * @param fileId
	 * @return
	 */
	int deleteFileInfo(String fileId);

	/**
	 * 软删除虚拟文件信息
	 * @param fileIds
	 * @return
	 */
	int deleteFiles(String[] fileIds);

	/**
	 * 软删除文件夹中的所有文件
	 * @param folderIds
	 * @return
	 */
	int deleteFilesInFolders(String[] folderIds);

	/**
	 * 更新一个虚拟文件信息
	 * @param info
	 * @return
	 */
	int updateFileInfo(FileInfo info);

	/**
	 * 将 store 关联的 file 设置为存在
	 * @param storeId
	 * @return
	 */
	int setFileExistsByStoreId(String storeId);

	/**
	 * 移动文件到指定文件夹
	 * @param fileIds
	 * @param folderId
	 * @return
	 */
	int moveFiles(@Param("fileIds") String[] fileIds,
	              @Param("folderId") String folderId);

	/**
	 * 获取某个虚拟文件的信息
	 * @param fileId
	 * @return
	 */
	FileInfo getFileInfo(String fileId);

	/**
	 * 列出指定目录下所有文件
	 * @param userId
	 * @param folderId
	 * @return
	 */
	List<FileInfo> listFilesInFolder(@Param("userId") String userId,
	                                 @Param("folderId") String folderId);

	/**
	 * 获取用户指定文件的存储地址
	 * @param userId
	 * @param fileId
	 * @return
	 */
	HashMap<String, Object> downloadFile(@Param("userId") String userId,
	                                     @Param("fileId") String fileId);
}
