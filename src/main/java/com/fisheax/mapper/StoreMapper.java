/**
 * @FileName FileMapper.java
 * @Package com.fisheax.mapper;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.mapper;

import com.fisheax.pojo.StoreInfo;

/**
 * @ClassName: FileMapper
 * @Description 文件操作 DAO
 * @Author fisheax
 * @Date 11/4/2016
 */
public interface StoreMapper
{
	/**
	 * 增加一条存储文件信息
	 * @param info
	 * @return
	 */
	int insertStoreInfo(StoreInfo info);

	/**
	 * 更新 store 信息, 暂时只允许修改 "是否存在"
	 * @param info
	 * @return
	 */
	int updateStoreInfo(StoreInfo info);

	/**
	 * 物理删除一条存储文件信息
	 * @param storeId
	 * @return
	 */
	int deleteStoreInfoPermanently(String storeId);

	/**
	 * 软删除一条存储文件信息
	 * @param storeId
	 * @return
	 */
	int deleteStoreInfo(String storeId);

	/**
	 * 获取指定存储文件信息
	 * @param storeId
	 * @return
	 */
	StoreInfo getStoreInfo(String storeId);
}
