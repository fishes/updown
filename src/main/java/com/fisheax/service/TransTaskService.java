/**
 * @FileName TransTaskService.java
 * @Package com.fisheax.service;
 * @Author fisheax
 * @Date 11/27/2016
 */
package com.fisheax.service;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.utils.EncyptUtil;
import com.fisheax.component.utils.StringUtil;
import com.fisheax.mapper.ChunkMapper;
import com.fisheax.mapper.FileMapper;
import com.fisheax.mapper.StoreMapper;
import com.fisheax.mapper.TransTaskMapper;
import com.fisheax.pojo.ChunkInfo;
import com.fisheax.pojo.FileInfo;
import com.fisheax.pojo.StoreInfo;
import com.fisheax.pojo.TransTaskInfo;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @ClassName: TransTaskService
 * @Description 上传下载任务相关服务
 * @Author fisheax
 * @Date 11/27/2016
 */
@Service
public class TransTaskService implements ApplicationContextAware
{
	private TransTaskMapper transTaskMapper;
	private ChunkMapper chunkMapper;
	private FileMapper fileMapper;
	private StoreMapper storeMapper;
	private ApplicationContext applicationContext;
	// taskId - { chunkId - { chunk infos ... }}
	@SuppressWarnings("unchecked")
	private ConcurrentHashMap<String, AutoMergedConcurrentHashMap> taskProgress = new ConcurrentHashMap();

	/**
	 * 创建上传任务
	 * @param folderId
	 * @param fileName
	 * @param fileHash
	 * @param fileSize
	 * @param userId
	 * @return
	 */
	@Transactional
	public ResultWrapper createUploadTask(String folderId, String fileName, String fileHash,
	                                      long fileSize, String userId)
	{
		StoreInfo storeInfo = new StoreInfo(StringUtil.uuid());
		storeInfo.setUserId(userId);
		storeInfo.setStoreHash(tellFileSaveFolder());
		storeInfo.setUpDone(0);
		storeInfo.setExists(0);
		storeInfo.setExtName("NO_NEED");
		storeMapper.insertStoreInfo(storeInfo);

		FileInfo fileInfo = new FileInfo(StringUtil.uuid());
		fileInfo.setFolderId(folderId);
		fileInfo.setStoreId(storeInfo.getStoreId());
		fileInfo.setFileName(fileName);
		fileInfo.setFileSize(fileSize);
		fileInfo.setExists(0);

		fileMapper.insertFileInfo(fileInfo);

		TransTaskInfo transTaskInfo = new TransTaskInfo(StringUtil.uuid());
		transTaskInfo.setStoreId(storeInfo.getStoreId());
		transTaskInfo.setFileHash(fileHash);
		transTaskInfo.setType(1);
		transTaskInfo.setIsDone(0);
		transTaskMapper.insertTransTaskInfo(transTaskInfo);

		ChunkInfo chunkInfo = new ChunkInfo(StringUtil.uuid());
		chunkInfo.setTaskId(transTaskInfo.getTaskId());
		chunkInfo.setStartPos(0);
		chunkInfo.setCurPos(0);
		chunkInfo.setEndPos(fileSize - 1);
		chunkInfo.setIsDone(0);
		chunkMapper.insertChunkInfo(chunkInfo);

		HashMap<String, Object> result = new HashMap<>();
		result.put("taskId", transTaskInfo.getTaskId());
		result.put("chunks", SubChunkInfo.packChunkInfos((Collections.singletonList(chunkInfo))));

		return ResultWrapper.success().succeed(result);
	}

	/**
	 * 得到上传文件的保存路径
	 * 也可以选择将绝对路径写到 MultipartConfig
	 * @return
	 */
	private String tellFileSaveFolder()
	{
		WebApplicationContext context = ((WebApplicationContext) applicationContext);
		final String webInfoFolder = context.getServletContext().getRealPath(File.separator) +
			File.separator + "WEB-INF" + File.separator + "files" + File.separator;

		String folderName = StringUtil.uuid().substring(0, 8) + File.separator;
		File folder = new File(webInfoFolder + folderName);

		if (!folder.exists())
			folder.mkdir();

		return webInfoFolder + folderName + StringUtil.uuid();
	}

	/**
	 * 保存文件分片
	 * @param chunk
	 * @param startPos
	 * @param chunkSize
	 * @param taskId
	 * @param chunkHash
	 * @return
	 */
	public ResultWrapper saveChunkToFolder(MultipartFile chunk, long startPos, long chunkSize,
	                                       String taskId, String chunkHash)
	{
		InputStream inputStream = null;
		FileOutputStream fileOutputStream = null;
		try
		{
			// may cause a huge problem because of trust chunk's size from the front end
			if (!chunkHash.equalsIgnoreCase(EncyptUtil.md5str(chunk.getBytes())))
				return ResultWrapper.failed("Invalid chunk hash").code(-512);

			String folder = tellChunkSaveFolder(taskId);
			File file = new File(folder + File.separator + startPos + "-" + (startPos + chunkSize - 1));
			if (!file.exists())
				file.createNewFile();

			byte[] buffer = new byte[2048];
			int length = 0;
			inputStream = chunk.getInputStream();
			fileOutputStream = new FileOutputStream(file);

			while ((length = inputStream.read(buffer)) != -1)
				fileOutputStream.write(buffer, 0, length);

			// to be sure the content would write into the disk
			// or not would cause the follow code can not obtain all chunks
			fileOutputStream.flush();
			fileOutputStream.getFD().sync();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
			return ResultWrapper.failed("Error while saving the chunks");
		}
		finally
		{
			try
			{
				if (inputStream != null)
					inputStream.close();
				if (fileOutputStream != null)
					fileOutputStream.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}

		String chunkId = StringUtil.uuid();
		taskProgress.computeIfAbsent(taskId, k -> new AutoMergedConcurrentHashMap())
			.put(chunkId, new SubChunkInfo(chunkId, startPos, 0, startPos + chunkSize - 1));

		return ResultWrapper.success("The chunk had been saved");
	}

	/**
	 * 获取某个分块的存储位置
	 * @param folderName
	 * @return
	 */
	private String tellChunkSaveFolder(String folderName)
	{
		WebApplicationContext context = ((WebApplicationContext) applicationContext);
		final String webInfoFolder = context.getServletContext().getRealPath(File.separator) +
			File.separator + "WEB-INF" + File.separator + "chunks" + File.separator;

		File folder = new File(webInfoFolder + folderName + File.separator);

		if (!folder.exists())
			folder.mkdir();

		return folder.getAbsolutePath();
	}

	/**
	 * 结束上传任务
	 * @param taskId
	 * @return
	 */
	@Transactional
	public ResultWrapper finishUploadTask(String taskId)
	{
		// all chunks -> done : no need, the records would be deleted
		AutoMergedConcurrentHashMap task = taskProgress.get(taskId);
		if (task == null)
			return ResultWrapper.failed("Invalid task id");
		task.persist(taskId, this);

		// task -> done
		StoreInfo storeInfo = transTaskMapper.getTaskStoreInfo(taskId);
		transTaskMapper.finishTransTask(taskId);

		// merge the chunks
		// here would cost a little time, may modify this behavior
		// -> merging while uploading, and may use task queue
		// -> do not let user waiting here but waiting at download while the task not finished
		File file = new File(storeInfo.getStoreHash());
		try (RandomAccessFile storeFile = new RandomAccessFile(file, "rws"))
		{
			if (!file.exists())
				file.createNewFile();

			File[] chunks = new File(tellChunkSaveFolder(taskId)).listFiles();
			if (chunks == null)
				return ResultWrapper.failed("No chunk saved in server");

			byte[] buffer = new byte[2048];
			int length = 0;
			for (File chunk : chunks)
			{
				try (FileInputStream fileInputStream = new FileInputStream(chunk))
				{
					// should be useless, because of reading files is in order
					// but sometimes it acts strangely, correct size with different hash
					long startPos = Long.parseLong(chunk.getName().split("-")[0]);
					storeFile.seek(startPos);

					while ((length = fileInputStream.read(buffer)) != -1)
						storeFile.write(buffer, 0, length);
				}
				catch (IOException ex)
				{
					ex.printStackTrace();
					return ResultWrapper.failed("Error while merge the chunks");
				}
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
			return ResultWrapper.failed("Error while merge the chunks");
		}

		// would be better to check the whole file hash to sure it is correct
		// but it would cost more time, here just ignore it

		// store info -> exists
		storeInfo.setExists(1);
		storeMapper.updateStoreInfo(storeInfo);

		// file info -> exists
		fileMapper.setFileExistsByStoreId(storeInfo.getStoreId());

		return ResultWrapper.success("The chunks merged successfully");
	}

	/**
	 * 停止上传任务
	 * @param taskId
	 * @return
	 */
	@Transactional
	public ResultWrapper stopUploadTask(String taskId)
	{
		// clean chunk progress info
		taskProgress.remove(taskId);
		chunkMapper.deleteChunkInfosByTaskId(taskId);

		// set task complete
		transTaskMapper.updateTransTaskInfo(new TransTaskInfo(taskId));
		return ResultWrapper.success("The task has stopped");
	}

	/**
	 * 创建下载任务
	 * @return
	 */
	@Transactional
	public ResultWrapper createDownloadTask()
	{
		// because of taking advice in HTTP protocol in design
		// read the coresponding file block by info send by client that support continuously transport
		// so it's no need to control the process by server
		return ResultWrapper.success("Temporarily no need to support this function");
	}

	/**
	 * 查询任务信息
	 * @param taskId
	 * @return
	 */
	@Transactional
	public ResultWrapper queryTaskInfo(String taskId)
	{
		// make all chunk info in the memory writed into database
		AutoMergedConcurrentHashMap task = taskProgress.get(taskId);
		if (task != null)
			task.persist(taskId, this);
		else
			taskProgress.put(taskId, new AutoMergedConcurrentHashMap());

		HashMap<String, Object> result = new HashMap<>();
		result.put("taskId", taskId);
		result.put("chunks", SubChunkInfo.packChunkInfos(chunkMapper.getUnfinishedChunksByTaskId(taskId)));
		return ResultWrapper.autoByNotEmpty(result.get("chunks")).succeed(result);
	}

	// ================================================================================================

	/**
	 * 自动合并加入的文件分片信息
	 * 这里保存的是已完成传输的文件分片区间
	 * 数据库里保存的是未完成的文件分片区间
	 */
	private static class AutoMergedConcurrentHashMap extends ConcurrentHashMap<String, SubChunkInfo>
	{
		/**
		 * put 的同时也在合并分片信息
		 * @param chunkId
		 * @param chunkInfo
		 * @return
		 */
		@Override
		public synchronized SubChunkInfo put(String chunkId, SubChunkInfo chunkInfo)
		{
			SubChunkInfo[] tmpInfo = new SubChunkInfo[1];
			String[] toRmChunkId = new String[1];

			forEach((k, v) ->
			{
				// it may could merge with the **next** chunk
				if (v.endPos + 1 == chunkInfo.startPos)
				{
					v.endPos = chunkInfo.endPos;
					tmpInfo[0] = v;

					forEach((ik, iv) ->
					{
						if (v.endPos + 1 == iv.startPos)
						{
							v.endPos = iv.endPos;
							toRmChunkId[0] = ik;
						}
					});
				}

				// it may could merge with the **previous** chunk
				if (chunkInfo.endPos + 1 == v.startPos)
				{
					v.startPos = chunkInfo.startPos;
					tmpInfo[0] = v;

					forEach((ik, iv) ->
					{
						if (iv.endPos + 1 == v.startPos)
						{
							v.startPos = iv.startPos;
							toRmChunkId[0] = ik;
						}
					});
				}
			});


			if (toRmChunkId[0] != null)
				remove(toRmChunkId[0]);

			return tmpInfo[0] == null ? super.put(chunkId, chunkInfo) : tmpInfo[0];
		}

		/**
		 * 将文件分片信息写回到数据库
		 * @param transTaskService
		 */
		void persist(String taskId, TransTaskService transTaskService)
		{
			int size = size();
			if (size == 0)
				return;

			ChunkMapper chunkMapper = transTaskService.getChunkMapper();
			TransTaskMapper transTaskMapper = transTaskService.getTransTaskMapper();
			SubChunkInfo[] infos = new SubChunkInfo[size];
			ArrayList<ChunkInfo> chunks = new ArrayList<>();
			long fileSize = transTaskMapper.getFileSizeByTaskId(taskId);

			values().toArray(infos);
			Arrays.sort(infos, (f, s) -> ((int) (f.startPos - s.startPos)));

			if (infos[0].startPos != 0)
				chunks.add(new ChunkInfo(StringUtil.uuid(), taskId,
					infos[0].startPos, 0, infos[0].endPos, 0));

			for (int index = 0; index < infos.length - 1; index++)
				chunks.add(new ChunkInfo(StringUtil.uuid(), taskId,
					infos[index].endPos + 1, 0, infos[index + 1].startPos - 1, 0));

			if (infos[size - 1].endPos + 1 != fileSize)
				chunks.add(new ChunkInfo(StringUtil.uuid(), taskId,
					infos[size - 1].endPos + 1, 0, fileSize - 1, 0));

			// delete all previous chunks info
			chunkMapper.deleteChunkInfosByTaskId(taskId);
			// and then insert the new ones (the task may complete so no need add more info
			if (chunks.size() != 0)
				chunkMapper.insertChunkInfos(chunks);
		}
	}

	// ================================================================================================

	@Autowired
	public TransTaskService(TransTaskMapper transTaskMapper, ChunkMapper chunkMapper,
	                        FileMapper fileMapper, StoreMapper storeMapper)
	{
		Assert.notNull(transTaskMapper);
		Assert.notNull(chunkMapper);
		Assert.notNull(fileMapper);
		Assert.notNull(storeMapper);

		this.transTaskMapper = transTaskMapper;
		this.chunkMapper = chunkMapper;
		this.fileMapper = fileMapper;
		this.storeMapper = storeMapper;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		this.applicationContext = applicationContext;
	}

	public TransTaskMapper getTransTaskMapper()
	{
		return transTaskMapper;
	}

	public ChunkMapper getChunkMapper()
	{
		return chunkMapper;
	}

	public FileMapper getFileMapper()
	{
		return fileMapper;
	}

	public StoreMapper getStoreMapper()
	{
		return storeMapper;
	}

	// ================================================================================================

	public static class SubChunkInfo
	{
		private String chunkId;
		private long startPos;
		private long curPos;
		private long endPos;

		/**
		 * 转换 ChunkInfo
		 * @param chunkInfos
		 * @return
		 */
		public static SubChunkInfo[] packChunkInfos(ChunkInfo[] chunkInfos)
		{
			if (chunkInfos == null)
				return new SubChunkInfo[0];

			return ((SubChunkInfo[]) packChunkInfos(Arrays.asList(chunkInfos)).toArray());
		}

		/**
		 * 转换 ChunkInfo
		 * @param chunkInfos
		 * @return
		 */
		public static List<SubChunkInfo> packChunkInfos(List<ChunkInfo> chunkInfos)
		{
			ArrayList<SubChunkInfo> infos = new ArrayList<>();

			if (chunkInfos == null || chunkInfos.size() == 0)
				return infos;

			return chunkInfos.stream().map(SubChunkInfo::new).collect(Collectors.toList());
		}

		// ================================================================================================

		public SubChunkInfo()
		{
		}

		public SubChunkInfo(ChunkInfo chunkInfo)
		{
			if (chunkInfo == null)
				return;

			this.chunkId = chunkInfo.getChunkId();
			this.startPos = chunkInfo.getStartPos();
			this.curPos = chunkInfo.getCurPos();
			this.endPos = chunkInfo.getEndPos();
		}

		public SubChunkInfo(String chunkId, long startPos, long curPos, long endPos)
		{
			this.chunkId = chunkId;
			this.startPos = startPos;
			this.curPos = curPos;
			this.endPos = endPos;
		}

		@Override
		public String toString()
		{
			return "SubChunkInfo{" +
				"chunkId='" + chunkId + '\'' +
				", startPos=" + startPos +
				", curPos=" + curPos +
				", endPos=" + endPos +
				'}';
		}

		public String getChunkId()
		{
			return chunkId;
		}

		public void setChunkId(String chunkId)
		{
			this.chunkId = chunkId;
		}

		public long getStartPos()
		{
			return startPos;
		}

		public void setStartPos(long startPos)
		{
			this.startPos = startPos;
		}

		public long getCurPos()
		{
			return curPos;
		}

		public void setCurPos(long curPos)
		{
			this.curPos = curPos;
		}

		public long getEndPos()
		{
			return endPos;
		}

		public void setEndPos(long endPos)
		{
			this.endPos = endPos;
		}
	}
}
