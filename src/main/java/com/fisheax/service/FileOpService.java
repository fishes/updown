/**
 * @FileName FileOpService.java
 * @Package com.fisheax.service;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.service;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.annotations.DoNotLogThisMethod;
import com.fisheax.component.utils.StringUtil;
import com.fisheax.mapper.FileMapper;
import com.fisheax.mapper.LogMapper;
import com.fisheax.mapper.StoreMapper;
import com.fisheax.pojo.FileInfo;
import com.fisheax.pojo.StoreInfo;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName: FileOpService
 * @Description 文件操作服务
 * @Author fisheax
 * @Date 11/4/2016
 */
@Service
public class FileOpService implements ApplicationContextAware
{
	private FileMapper fileMapper;
	private StoreMapper storeMapper;
	private LogMapper logMapper;
	private ApplicationContext applicationContext;

	/**
	 * 创建文件
	 * @param userId
	 * @param dstFolderId
	 * @param files
	 * @return
	 */
	@DoNotLogThisMethod
	@Transactional
	public ResultWrapper createFiles(String userId, String dstFolderId, List<MultipartFile> files)
	{
		List<FileInfo> fileInfos = new ArrayList<>(files.size());
		List<StoreInfo> storeInfos = new ArrayList<>(files.size());

		files.forEach(f -> {
			String storeFile = tellTargetFolder();
			try
			{
				File file = new File(storeFile);
				if (!file.exists())
					file.createNewFile();

				f.transferTo(file);
			}
			catch (IOException e) { return; }

			StoreInfo storeInfo = new StoreInfo(StringUtil.uuid());
			storeInfo.setUserId(userId);
			// 这里所谓的 hash 其实是文件路径
			storeInfo.setStoreHash(storeFile);
			storeInfo.setUpDone(1);
			storeInfo.setExists(1);
			int extDotPos = f.getOriginalFilename().lastIndexOf('.');
			storeInfo.setExtName(extDotPos == -1 ? "" : f.getOriginalFilename().substring(extDotPos));

			storeInfos.add(storeInfo);

			FileInfo fileInfo = new FileInfo(StringUtil.uuid());
			fileInfo.setFolderId(dstFolderId);
			fileInfo.setStoreId(storeInfo.getStoreId());
			fileInfo.setFileName(f.getOriginalFilename());
			fileInfo.setFileSize(f.getSize());
			fileInfo.setExists(1);

			fileInfos.add(fileInfo);
		});

		storeInfos.forEach(storeMapper::insertStoreInfo);
		fileInfos.forEach(fileMapper::insertFileInfo);

		return ResultWrapper.success("The files had been saved");
	}

	/**
	 * 用户个人文件下载
	 * @param userId
	 * @param fileId
	 * @return
	 */
	@DoNotLogThisMethod
	@Transactional
	public ResultWrapper downloadFile(String userId, String fileId)
	{
		HashMap<String, Object> result = fileMapper.downloadFile(userId, fileId);
		if (result == null || result.size() == 0)
			return ResultWrapper.failed("The file was deleted or recycled");

		return ResultWrapper.success().succeed(result);
	}

	/**
	 * 拷贝文件到指定文件夹
	 * @param fileIds
	 * @param dstFolderId
	 * @return
	 */
	@Transactional
	public ResultWrapper copyFilesTo(String[] fileIds, String dstFolderId)
	{
		return ResultWrapper.autoByNotNull(fileMapper.copyFiles(fileIds, dstFolderId));
	}

	/**
	 * 移动文件到指定文件夹
	 * @param fileIds
	 * @param dstFolderId
	 * @return
	 */
	@Transactional
	public ResultWrapper moveFilesTo(String[] fileIds, String dstFolderId)
	{
		return ResultWrapper.autoByEquals(fileMapper.moveFiles(fileIds, dstFolderId), fileIds.length);
	}

	/**
	 * 删除文件
	 * @param fileIds
	 * @return
	 */
	@Transactional
	public ResultWrapper deleteFiles(String[] fileIds)
	{
		return ResultWrapper.autoByEquals(fileMapper.deleteFiles(fileIds), fileIds.length);
	}

	/**
	 * 重命名一个虚拟文件
	 * @param fileId
	 * @param newName
	 * @return
	 */
	@Transactional
	public ResultWrapper renameFile(String fileId, String newName)
	{
		FileInfo file = new FileInfo(fileId);
		file.setFileName(newName);

		return ResultWrapper.autoByEquals(fileMapper.updateFileInfo(file), 1);
	}

	/**
	 * 列出指定目录下的所有文件
	 * @param userId
	 * @param parentFolderId
	 * @return
	 */
	public ResultWrapper listFiles(String userId, String parentFolderId)
	{
		return ResultWrapper.autoByNotNull(fileMapper.listFilesInFolder(userId, parentFolderId));
	}

	// ================================================================================================

	/**
	 * 得到上传文件的保存路径
	 * 也可以选择将绝对路径写到 MultipartConfig
	 * @return
	 */
	private String tellTargetFolder()
	{
		WebApplicationContext context = ((WebApplicationContext) applicationContext);
		final String webInfoFolder = context.getServletContext().getRealPath(File.separator) +
			File.separator + "WEB-INF" + File.separator + "files" + File.separator;

		String folderName = StringUtil.uuid().substring(0, 8) + File.separator;
		File folder = new File(webInfoFolder + folderName);

		if (!folder.exists())
			folder.mkdir();

		return webInfoFolder + folderName + StringUtil.uuid();
	}

	// ================================================================================================

	@Autowired
	public FileOpService(FileMapper fileMapper, StoreMapper storeMapper, LogMapper logMapper)
	{
		Assert.notNull(fileMapper);
		Assert.notNull(storeMapper);
		Assert.notNull(logMapper);

		this.fileMapper = fileMapper;
		this.storeMapper = storeMapper;
		this.logMapper = logMapper;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		this.applicationContext = applicationContext;
	}

	public FileMapper getFileMapper()
	{
		return fileMapper;
	}

	public StoreMapper getStoreMapper()
	{
		return storeMapper;
	}
}
