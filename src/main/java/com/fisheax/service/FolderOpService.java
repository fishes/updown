/**
 * @FileName FolderOpService.java
 * @Package com.fisheax.service;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.service;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.annotations.DoNotLogThisMethod;
import com.fisheax.component.utils.StringUtil;
import com.fisheax.mapper.FileMapper;
import com.fisheax.mapper.FolderMapper;
import com.fisheax.mapper.LogMapper;
import com.fisheax.pojo.FileInfo;
import com.fisheax.pojo.FolderInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;

/**
 * @ClassName: FolderOpService
 * @Description 文件夹操作服务
 * @Author fisheax
 * @Date 11/7/2016
 */
@Service
public class FolderOpService
{
	private FolderMapper folderMapper;
	private FileMapper fileMapper;
	private LogMapper logMapper;

	/**
	 * 创建一个虚拟目录
	 * @param parentFolderId
	 * @param folderName
	 * @return
	 */
	@DoNotLogThisMethod
	@Transactional
	public ResultWrapper createFolder(String parentFolderId, String userId, String folderName)
	{
		FolderInfo folderInfo = new FolderInfo(StringUtil.uuid());
		folderInfo.setFolderPid(parentFolderId);
		folderInfo.setUserId(userId);
		folderInfo.setFolderName(folderName);
		folderInfo.setExists(1);

		return ResultWrapper.autoByEquals(folderMapper.insertFolderInfo(folderInfo), 1);
	}

	/**
	 * 拷贝目录到指定目录下
	 * @param folderIds
	 * @param dstFolderId
	 * @return
	 */
	@Transactional
	public ResultWrapper copyFoldersTo(String[] folderIds, String dstFolderId)
	{
		return ResultWrapper.autoByEquals(folderMapper.copyFolders(new HashMap<String, Object>(){{
				put("folderPid", dstFolderId);
				put("folderIds", StringUtil.join(folderIds));
			}}), folderIds.length, "copy folders success", "unkown error");
	}

	/**
	 * 移动目录到指定目录下
	 * @param folderIds
	 * @param dstFolderId
	 * @return
	 */
	@Transactional
	public ResultWrapper moveFoldersTo(String[] folderIds, String dstFolderId)
	{
		return ResultWrapper.autoByEquals(folderMapper.moveFolders(folderIds, dstFolderId), folderIds.length,
			"move folders success", "unkown error");
	}

	/**
	 * 删除目录及目录底下文件
	 * 注意: 不会递归删除底下的子文件夹及子文件夹中的文件
	 * @param folderIds
	 * @return
	 */
	@DoNotLogThisMethod
	@Transactional
	public ResultWrapper deleteFolders(String[] folderIds)
	{
		if (ResultWrapper.autoByEquals(folderMapper.deleteFolders(StringUtil.join(folderIds)), folderIds.length,
			"delete folders success", "unkown error").succeed())
			return ResultWrapper.autoByNotEquals(fileMapper.deleteFiles(folderIds), -1,
				"delete folders success", "unkown error");

		return ResultWrapper.failed();
	}

	/**
	 * 重命名一个文件夹
	 * @param folderId
	 * @param newFolderName
	 * @return
	 */
	@Transactional
	public ResultWrapper renameFolder(String folderId, String newFolderName)
	{
		FolderInfo folderInfo = new FolderInfo(folderId);
		folderInfo.setFolderName(newFolderName);
		// set level -1 means not to change level
		folderInfo.setLevel(-1);

		return ResultWrapper.autoByEquals(folderMapper.updateFolderInfo(folderInfo), 1,
			"rename folder success", "unkown error");
	}

	/**
	 * 列出指定下所有一级子目录及文件
	 * @param parentFolderId
	 * @return
	 */
	public ResultWrapper listSubFoldersAndFiles(String parentFolderId)
	{
		List<FolderInfo> folders = folderMapper.getSubFoldersById(parentFolderId);
		List<FileInfo> files = folderMapper.getFilesInDir(parentFolderId);

		return ResultWrapper.success().succeed(new HashMap<String, List<?>>()
		{{
			put("folders", folders);
			put("files", files);
		}});
	}

	/**
	 * 列出指定下所有一级子目录
	 * @param parentFolderId
	 * @return
	 */
	public ResultWrapper listSubFolders(String parentFolderId)
	{
		return ResultWrapper.success()
			.succeed(folderMapper.getSubFoldersById(parentFolderId));
	}

	/**
	 * 获取用户的根目录信息
	 * @param userId
	 * @return
	 */
	public ResultWrapper getRootFolderInfo(String userId)
	{
		FolderInfo info = folderMapper.getRootFolderInfo(userId);
		return ResultWrapper.autoByNotNull(info).succeed(info);
	}

	/**
	 * 列出指定用户的目录树
	 * @param userId
	 * @return
	 */
	public ResultWrapper getFolderStructure(String userId, String[] filteredIds)
	{
		HashMap<String, Object> params = new HashMap<String, Object>() {{
			put("userId", userId);
			put("filteredFolderIds", StringUtil.join(filteredIds));
		}};

		folderMapper.getFolderStruture(params);
		String result = ((String) params.get("result"));

		return ResultWrapper.autoByNotEmpty(result).succeed(result);
	}

	// ================================================================================================

	@Autowired
	public FolderOpService(FolderMapper folderMapper, FileMapper fileMapper, LogMapper logMapper)
	{
		Assert.notNull(folderMapper);
		Assert.notNull(fileMapper);
		Assert.notNull(logMapper);

		this.folderMapper = folderMapper;
		this.fileMapper = fileMapper;
		this.logMapper = logMapper;
	}
}
