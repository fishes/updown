/**
 * @FileName UserService.java
 * @Package com.fisheax.service;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.service;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.annotations.DoNotLogThisMethod;
import com.fisheax.component.utils.EncyptUtil;
import com.fisheax.component.utils.StringUtil;
import com.fisheax.mapper.FolderMapper;
import com.fisheax.mapper.LogMapper;
import com.fisheax.mapper.UserMapper;
import com.fisheax.pojo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @ClassName: UserService
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@Service
public class UserService
{
	private UserMapper userMapper;
	private FolderMapper folderMapper;
	private LogMapper logMapper;

	/**
	 * 用户注册
	 * @param info
	 */
	@DoNotLogThisMethod
	@Transactional
	public ResultWrapper register(UserInfo info)
	{
		info.setUserId(StringUtil.uuid());
		if (userMapper.isUsernameOccupied(info.getUsername()))
			return ResultWrapper.failed("The username is occupied");

		if (ResultWrapper.autoByEquals(userMapper.insertUserInfo(encypt(info)), 1).succeed())
			// 注册成功后创建用户的根目录
			return ResultWrapper.autoByEquals(folderMapper.createRootFolder(info.getUserId()), 1,
				"register success", "unkown error");

		return ResultWrapper.failed();
	}

	/**
	 * 用户登录, 验证 name + pass
	 * @param info
	 * @return
	 */
	@DoNotLogThisMethod
	public ResultWrapper login(UserInfo info)
	{
		return ResultWrapper.auto(userMapper.isUserValid(encypt(info)), "login success", "unknown error")
			.succeed(userMapper.getUserInfoByName(info.getUsername()));
	}

	/**
	 * 用户登录, 验证 App key
	 * @param appSecret
	 * @return
	 */
	@DoNotLogThisMethod
	public ResultWrapper authenticate(String appSecret)
	{
		return ResultWrapper.auto(userMapper.isKeyValid(appSecret));
	}

	// ================================================================================================

	// 置/取盐, 加密 password
	private UserInfo encypt(UserInfo info)
	{
		if (info == null)
			return null;

		UserInfo user = userMapper.getUserInfoByName(info.getUsername());
		String salt = user == null ? StringUtil.uuid().substring(0, 8) : user.getSalt();

		info.setSalt(salt);
		info.setPassword(EncyptUtil.md5(salt + info.getPassword() + salt));

		return info;
	}

	// ================================================================================================

	@Autowired
	public UserService(UserMapper userMapper, FolderMapper folderMapper, LogMapper logMapper)
	{
		Assert.notNull(userMapper);
		Assert.notNull(folderMapper);
		Assert.notNull(logMapper);

		this.userMapper = userMapper;
		this.folderMapper = folderMapper;
		this.logMapper = logMapper;
	}

	public UserMapper getUserMapper()
	{
		return userMapper;
	}
}
