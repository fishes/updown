/**
 * @FileName CleanChunkTask.java
 * @Package com.fisheax.tasks;
 * @Author fisheax
 * @Date 11/10/2016
 */
package com.fisheax.tasks;

import org.springframework.stereotype.Component;

/**
 * @ClassName: CleanChunkTask
 * @Description 异常断点续传清理
 * @Author fisheax
 * @Date 11/10/2016
 */
@Component
public class CleanChunkTask
{
//	@Scheduled(cron = "0/10 * * * * ?")
//	public void clean()
//	{
//		System.out.println("clean Chunk");
//	}
}
