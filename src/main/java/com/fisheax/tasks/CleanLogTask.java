/**
 * @FileName CleanLogTask.java
 * @Package com.fisheax.tasks;
 * @Author fisheax
 * @Date 11/10/2016
 */
package com.fisheax.tasks;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @ClassName: CleanLogTask
 * @Description 日志清理
 * @Author fisheax
 * @Date 11/10/2016
 */
@Component
public class CleanLogTask
{
	@Scheduled(cron = "0/18000 * * * * ?")
	public void clean()
	{
	}
}
