/**
 * @FileName LogInterceptor.java
 * @Package com.fisheax.interceptor;
 * @Author fisheax
 * @Date 11/14/2016
 */
package com.fisheax.interceptor;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.utils.LogUtil;
import com.fisheax.component.utils.StringUtil;
import com.fisheax.mapper.LogMapper;
import com.fisheax.pojo.LogInfo;
import com.fisheax.pojo.UserInfo;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @ClassName: LogInterceptor
 * @Description 日志记录
 * @Author fisheax
 * @Date 11/14/2016
 */
@Component
@Aspect
public class LogInterceptor
{
	// 记录 service 包中所有的 public 方法
	@Pointcut("execution(public * com.fisheax.service..*(..))")
	private void serviceMethod(){}

	// 不记录 @DoNotLogThisMethod 标注的方法
	// @DoNotLogThisMethod 用于自己控制日志记录
	@Pointcut("!@annotation(com.fisheax.component.annotations.DoNotLogThisMethod)")
	private void notLogThisMethod(){}

	// 不记录 service 包中所有的 list 开头的方法
	@Pointcut("!execution(public * com.fisheax.service.*.list*(..))")
	private void notLogListMethod(){}

	// 不记录 service 包中所有的 get 开头的方法
	@Pointcut("!execution(public * com.fisheax.service.*.get*(..))")
	private void notLogGetMethod(){}

	private LogMapper logMapper;

	/**
	 * 正常返回简单日志记录
	 * @param point
	 * @param result
	 */
	@AfterReturning(value = "serviceMethod() && notLogThisMethod() && notLogGetMethod() && notLogListMethod()",
		returning = "result")
	public void whenReturn(JoinPoint point, ResultWrapper result)
	{
		// the parameter may be large object, it is a problem
		// so here just stopped to log the system.
		// The same to the next method logging
		// serializeLogInfo(point, result, "Return-Method : ");
	}

	/**
	 * 异常返回简单日志记录
	 * @param point
	 * @param ex
	 */
	@AfterThrowing(value = "serviceMethod() && notLogThisMethod()", throwing = "ex")
	public void whenException(JoinPoint point, Throwable ex)
	{
		// serializeLogInfo(point, null, "Return-Method : ");
	}

	// ================================================================================================

	// 生成 LogInfo 实例
	private LogInfo createLogInfo(JoinPoint point, ResultWrapper result)
	{
		String userId = ((UserInfo) ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
			.getRequest().getSession().getAttribute("user")).getUserId();
		Object[] args = point.getArgs();

		return args.length == 1 && isPojo(args[0]) ?
			LogUtil.get(userId, LogUtil.OpType.UNKOWN, result != null && result.succeed(), null, args[0]) :
			LogUtil.get(userId, LogUtil.OpType.UNKOWN, result != null && result.succeed(),
				"(" + StringUtil.removeHeadAndTail(LogUtil.strinifyWithName(false, args)) + ")", null);
	}

	// 持久化 LogInfo
	private void serializeLogInfo(JoinPoint point, ResultWrapper result, String extra)
	{
		LogInfo logInfo = createLogInfo(point, result);
		String ts = pickShortMethodName(point.getSignature().toString());
		if (extra != null)
			ts = extra + ts;
		logInfo.setOpDetail(ts + logInfo.getOpDetail());
		logMapper.insertLogInfo(logInfo);
	}

	// 处理方法签名显示形式
	private String pickShortMethodName(String signature)
	{
		int retTypePos = signature.indexOf(" ");
		int paramPos = signature.lastIndexOf("(");
		return signature.substring(retTypePos + 1, paramPos);
	}

	// 判定是否为 POJO
	private boolean isPojo(Object object)
	{
		return object != null && object.getClass().getSimpleName().endsWith("Info");
	}

	// ================================================================================================

	@Autowired
	public LogInterceptor(LogMapper logMapper)
	{
		Assert.notNull(logMapper);

		this.logMapper = logMapper;
	}
}
