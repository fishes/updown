/**
 * @FileName UserInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.pojo;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @ClassName: UserInfo
 * @Description 用户信息
 * @Author fisheax
 * @Date 11/4/2016
 */
public class UserInfo
{
	/*
	CREATE TABLE updown.ud_user_info (
		udui_id varchar(32) NOT NULL COMMENT '用户信息ID',
		udui_name varchar(255) NOT NULL COMMENT '用户名称',
		udui_pass varchar(255) NOT NULL COMMENT '用户密码',
		udui_salt varchar(8) NOT NULL COMMENT '盐',
		udui_app_secret varchar(255) NOT NULL COMMENT 'App端操作凭证',
		PRIMARY KEY (udui_id)
	) COMMENT = '用户信息'
	 */

	private String userId;
	@NotBlank
	private String username;
	@NotBlank
	private String password;
	private String salt;
	private String secret;

	public UserInfo()
	{
	}

	public UserInfo(String userId)
	{
		this.userId = userId;
	}

	@Override
	public String toString()
	{
		return "UserInfo{" +
			"userId='" + userId + '\'' +
			", username='" + username + '\'' +
			", password='" + password + '\'' +
			", salt='" + salt + '\'' +
			", secret='" + secret + '\'' +
			'}';
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getSalt()
	{
		return salt;
	}

	public void setSalt(String salt)
	{
		this.salt = salt;
	}

	public String getSecret()
	{
		return secret;
	}

	public void setSecret(String secret)
	{
		this.secret = secret;
	}
}
