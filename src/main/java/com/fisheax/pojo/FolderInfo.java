/**
 * @FileName FolderInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.pojo;

/**
 * @ClassName: FolderInfo
 * @Description 虚拟目录信息
 * @Author fisheax
 * @Date 11/4/2016
 */
public class FolderInfo
{
	/*
	CREATE TABLE updown.ud_dir_info (
		uddi_id varchar(32) NOT NULL COMMENT '目录ID',
		uddi_pid varchar(32) NOT NULL COMMENT '父目录ID FK:uddi_id',
		uddi_user_id varchar(32) NOT NULL COMMENT '用户ID FK:udui_id',
		uddi_name varchar(50) NOT NULL COMMENT '目录名称',
		uddi_level int(11) UNSIGNED NOT NULL COMMENT '目录层级',
		uddi_exists tinyint(4) NOT NULL COMMENT '是否已删除',
		PRIMARY KEY (uddi_id)
	) COMMENT = '虚拟文件目录信息'
	 */

	private String folderId;
	private String folderPid;
	private String userId;
	private String folderName;
	private int level;
	private int exists;

	public FolderInfo()
	{
	}

	public FolderInfo(String folderId)
	{
		this.folderId = folderId;
	}

	@Override
	public String toString()
	{
		return "FolderInfo{" +
			"folderId='" + folderId + '\'' +
			", folderPid='" + folderPid + '\'' +
			", folderName='" + folderName + '\'' +
			", level='" + level + '\'' +
			", exists=" + exists +
			'}';
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getFolderId()
	{
		return folderId;
	}

	public void setFolderId(String folderId)
	{
		this.folderId = folderId;
	}

	public String getFolderPid()
	{
		return folderPid;
	}

	public void setFolderPid(String folderPid)
	{
		this.folderPid = folderPid;
	}

	public String getFolderName()
	{
		return folderName;
	}

	public void setFolderName(String folderName)
	{
		this.folderName = folderName;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public int getExists()
	{
		return exists;
	}

	public void setExists(int exists)
	{
		this.exists = exists;
	}
}
