/**
 * @FileName TransTaskInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/21/2016
 */
package com.fisheax.pojo;

/**
 * @ClassName: TransTaskInfo
 * @Description
 * @Author fisheax
 * @Date 11/21/2016
 */
public class TransTaskInfo
{
	/*
	 CREATE TABLE IF NOT EXISTS ud_trans_task (
		 udtt_id varchar(32) NOT NULL COMMENT '任务ID',
		 udtt_store_id varchar(32) NOT NULL COMMENT '存储文件ID FK:udsi_id',
		 udtt_file_hash varchar(255) NOT NULL COMMENT '传输文件hash',
		 udtt_type tinyint(4) NOT NULL COMMENT '任务传输类型 0: 下载 1: 上传',
		 udtt_is_done tinyint(4) NOT NULL COMMENT '任务是否完成 0: 未完成 1: 已完成',
		 PRIMARY KEY (udtt_id)
	 ) COMMENT = '分块断点续传任务信息'
	 */

	private String taskId;
	private String storeId;
	private String fileHash;
	private int type;
	private int isDone;

	public TransTaskInfo()
	{
	}

	public TransTaskInfo(String taskId)
	{
		this.taskId = taskId;
	}

	@Override
	public String toString()
	{
		return "TransTaskInfo{" +
			"taskId='" + taskId + '\'' +
			", storeId='" + storeId + '\'' +
			", fileHash='" + fileHash + '\'' +
			", type=" + type +
			", isDone=" + isDone +
			'}';
	}

	public String getTaskId()
	{
		return taskId;
	}

	public void setTaskId(String taskId)
	{
		this.taskId = taskId;
	}

	public String getStoreId()
	{
		return storeId;
	}

	public void setStoreId(String storeId)
	{
		this.storeId = storeId;
	}

	public String getFileHash()
	{
		return fileHash;
	}

	public void setFileHash(String fileHash)
	{
		this.fileHash = fileHash;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public int getIsDone()
	{
		return isDone;
	}

	public void setIsDone(int isDone)
	{
		this.isDone = isDone;
	}
}
