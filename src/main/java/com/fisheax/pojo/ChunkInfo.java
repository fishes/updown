/**
 * @FileName ChunkInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/21/2016
 */
package com.fisheax.pojo;

import java.sql.Timestamp;

/**
 * @ClassName: ChunkInfo
 * @Description
 * @Author fisheax
 * @Date 11/21/2016
 */
public class ChunkInfo
{
	/*
	CREATE TABLE IF NOT EXISTS ud_chunk (
		udc_id varchar(32) NOT NULL COMMENT '断点续传信息ID',
		udc_task_id varchar(32) NOT NULL COMMENT '传输任务ID FK:udtt_id',
		udc_start_pos int(11) UNSIGNED NOT NULL COMMENT '区块开始位移',
		udc_cur_pos int(11) UNSIGNED NOT NULL COMMENT '传输结束位移',
		udc_end_pos int(11) UNSIGNED NOT NULL COMMENT '区块末尾位移',
		udc_is_done tinyint(4) NOT NULL COMMENT '区块是否传输完成 0: 未完成 1: 已完成',
		udc_last_mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '区块最后的传输时间',
		PRIMARY KEY (udc_id)
	) COMMENT = '断点续传信息'
	*/

	private String chunkId;
	private String taskId;
	private long startPos;
	private long curPos;
	private long endPos;
	private int isDone;
	private Timestamp lastTransTime;

	public ChunkInfo()
	{
	}

	public ChunkInfo(String chunkId)
	{
		this.chunkId = chunkId;
	}

	public ChunkInfo(String chunkId, String taskId, long startPos, long curPos, long endPos, int isDone)
	{
		this.chunkId = chunkId;
		this.taskId = taskId;
		this.startPos = startPos;
		this.curPos = curPos;
		this.endPos = endPos;
		this.isDone = isDone;
	}

	@Override
	public String toString()
	{
		return "ChunkInfo{" +
			"chunkId='" + chunkId + '\'' +
			", taskId='" + taskId + '\'' +
			", startPos=" + startPos +
			", curPos=" + curPos +
			", endPos=" + endPos +
			", isDone=" + isDone +
			", lastTransTime=" + lastTransTime +
			'}';
	}

	public String getChunkId()
	{
		return chunkId;
	}

	public void setChunkId(String chunkId)
	{
		this.chunkId = chunkId;
	}

	public String getTaskId()
	{
		return taskId;
	}

	public void setTaskId(String taskId)
	{
		this.taskId = taskId;
	}

	public long getStartPos()
	{
		return startPos;
	}

	public void setStartPos(long startPos)
	{
		this.startPos = startPos;
	}

	public long getCurPos()
	{
		return curPos;
	}

	public void setCurPos(long curPos)
	{
		this.curPos = curPos;
	}

	public long getEndPos()
	{
		return endPos;
	}

	public void setEndPos(long endPos)
	{
		this.endPos = endPos;
	}

	public int getIsDone()
	{
		return isDone;
	}

	public void setIsDone(int isDone)
	{
		this.isDone = isDone;
	}

	public Timestamp getLastTransTime()
	{
		return lastTransTime;
	}

	public void setLastTransTime(Timestamp lastTransTime)
	{
		this.lastTransTime = lastTransTime;
	}
}
