/**
 * @FileName FileInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.pojo;

import java.sql.Timestamp;

/**
 * @ClassName: FileInfo
 * @Description 虚拟文件信息
 * @Author fisheax
 * @Date 11/4/2016
 */
public class FileInfo
{
	/*
	CREATE TABLE updown.ud_file_info (
		udfi_id varbinary(32) NOT NULL COMMENT '虚拟文件信息ID',
		udfi_store_id varchar(32) NOT NULL COMMENT '文件存储信息ID FK:udsi_id',
		udfi_dir_id varchar(32) NOT NULL COMMENT '文件目录信息ID FK:uddi_id',
		udfi_name varchar(50) NOT NULL COMMENT '文件名称',
		udfi_size int(11) NOT NULL COMMENT '文件大小',
		udfi_up_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文件上传时间',
		udfi_exists tinyint(4) NOT NULL COMMENT '是否被删除（非物理）',
		PRIMARY KEY (udfi_id)
	) COMMENT = '虚拟文件信息'
	 */

	private String fileId;
	private String storeId;
	private String folderId;
	private String fileName;
	private long fileSize;
	private Timestamp uploadTime;
	private int exists;

	public FileInfo()
	{
	}

	public FileInfo(String fileId)
	{
		this.fileId = fileId;
	}

	@Override
	public String toString()
	{
		return "FileInfo{" +
			"fileId='" + fileId + '\'' +
			", storeId='" + storeId + '\'' +
			", folderId='" + folderId + '\'' +
			", fileName='" + fileName + '\'' +
			", fileSize='" + fileSize + '\'' +
			", uploadTime=" + uploadTime +
			", exists=" + exists +
			'}';
	}

	public String getFileId()
	{
		return fileId;
	}

	public void setFileId(String fileId)
	{
		this.fileId = fileId;
	}

	public String getStoreId()
	{
		return storeId;
	}

	public void setStoreId(String storeId)
	{
		this.storeId = storeId;
	}

	public String getFolderId()
	{
		return folderId;
	}

	public void setFolderId(String folderId)
	{
		this.folderId = folderId;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public long getFileSize()
	{
		return fileSize;
	}

	public void setFileSize(long fileSize)
	{
		this.fileSize = fileSize;
	}

	public Timestamp getUploadTime()
	{
		return uploadTime;
	}

	public void setUploadTime(Timestamp uploadTime)
	{
		this.uploadTime = uploadTime;
	}

	public int getExists()
	{
		return exists;
	}

	public void setExists(int exists)
	{
		this.exists = exists;
	}
}
