/**
 * @FileName StoreInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.pojo;

import java.sql.Timestamp;

/**
 * @ClassName: StoreInfo
 * @Description 文件存储信息
 * @Author fisheax
 * @Date 11/4/2016
 */
public class StoreInfo
{
	/*
	CREATE TABLE updown.ud_store_info (
		udsi_id varchar(32) NOT NULL COMMENT '存储文件信息ID',
		udsi_user_id varchar(32) NOT NULL COMMENT '存储文件所属用户ID FK:udui_id',
		udsi_hash varchar(255) NOT NULL COMMENT '存储文件名称/hash',
	    udsi_ext_name varchar(255) NOT NULL DEFAULT '' COMMENT '文件扩展名',
		udsi_exists tinyint(4) NOT NULL COMMENT '存储文件是否被删除',
		udsi_up_done tinyint(4) NOT NULL DEFAULT 1 COMMENT '文件是否上传完成',
	    udsi_last_mtime timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文件最后修改时间',
		PRIMARY KEY (udsi_id)
	) COMMENT = '实际文件存储信息'
	 */

	private String storeId;
	private String userId;
	private String storeHash;
	private String extName;
	private int exists;
	private int upDone;
	private Timestamp lastModifiedTime;

	public StoreInfo()
	{
	}

	public StoreInfo(String storeId)
	{
		this.storeId = storeId;
	}

	@Override
	public String toString()
	{
		return "StoreInfo{" +
			"storeId='" + storeId + '\'' +
			", userId='" + userId + '\'' +
			", storeHash='" + storeHash + '\'' +
			", extName='" + extName + '\'' +
			", exists=" + exists +
			", upDone=" + upDone +
			'}';
	}

	public Timestamp getLastModifiedTime()
	{
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime)
	{
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExtName()
	{
		return extName;
	}

	public void setExtName(String extName)
	{
		this.extName = extName;
	}

	public String getStoreId()
	{
		return storeId;
	}

	public void setStoreId(String storeId)
	{
		this.storeId = storeId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getStoreHash()
	{
		return storeHash;
	}

	public void setStoreHash(String storeHash)
	{
		this.storeHash = storeHash;
	}

	public int getExists()
	{
		return exists;
	}

	public void setExists(int exists)
	{
		this.exists = exists;
	}

	public int getUpDone()
	{
		return upDone;
	}

	public void setUpDone(int upDone)
	{
		this.upDone = upDone;
	}
}
