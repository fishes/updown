/**
 * @FileName LogInfo.java
 * @Package com.fisheax.pojo;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.pojo;

import java.sql.Timestamp;

/**
 * @ClassName: LogInfo
 * @Description 操作日志信息
 * @Author fisheax
 * @Date 11/4/2016
 */
public class LogInfo
{
	/*
	CREATE TABLE updown.ud_logs (
		udl_id varchar(32) NOT NULL COMMENT '日志记录ID',
		udl_op_id varchar(32) NOT NULL COMMENT '操作人ID FK:udui_id',
		udl_op_type varchar(50) NOT NULL COMMENT '操作类型',
		udl_op_detail varchar(2000) NOT NULL COMMENT '操作日志明细',
		udl_op_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
	    udl_op_result tinyint NOT NULL COMMENT '操作结果 0 success -1 failure',
		PRIMARY KEY (udl_id)
	) COMMENT = '操作日志记录'
	 */

	private String logId;
	private String opId;
	private String opType;
	private String opDetail;
	private Timestamp opTime;
	private int opResult;

	public LogInfo()
	{
	}

	public LogInfo(String logId)
	{
		this.logId = logId;
	}

	@Override
	public String toString()
	{
		return "LogInfo{" +
			"logId='" + logId + '\'' +
			", opId='" + opId + '\'' +
			", opType='" + opType + '\'' +
			", opDetail='" + opDetail + '\'' +
			", opTime=" + opTime +
			'}';
	}

	public String getLogId()
	{
		return logId;
	}

	public void setLogId(String logId)
	{
		this.logId = logId;
	}

	public String getOpId()
	{
		return opId;
	}

	public void setOpId(String opId)
	{
		this.opId = opId;
	}

	public String getOpType()
	{
		return opType;
	}

	public void setOpType(String opType)
	{
		this.opType = opType;
	}

	public String getOpDetail()
	{
		return opDetail;
	}

	public void setOpDetail(String opDetail)
	{
		this.opDetail = opDetail;
	}

	public Timestamp getOpTime()
	{
		return opTime;
	}

	public void setOpTime(Timestamp opTime)
	{
		this.opTime = opTime;
	}

	public int getOpResult()
	{
		return opResult;
	}

	public void setOpResult(int opResult)
	{
		this.opResult = opResult;
	}
}
