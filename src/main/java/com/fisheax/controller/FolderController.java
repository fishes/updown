/**
 * @FileName FolderController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.controller;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.checker.FolderValidationChecker;
import com.fisheax.component.checker.defaults.DefaultFolderValidationChecker;
import com.fisheax.pojo.UserInfo;
import com.fisheax.service.FolderOpService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @ClassName: FolderController
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@Controller
@RequestMapping("/folder")
public class FolderController implements ApplicationContextAware
{
	private FolderOpService folderOpService;
	private FolderValidationChecker folderValidationChecker;

	/**
	 * 创建文件夹
	 * @param folderPid
	 * @param folderName
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResultWrapper createFolder(@RequestParam("folderPid") String folderPid,
	                                  @RequestParam("folderName") String folderName,
	                                  HttpSession session)
	{
		UserInfo user = (UserInfo) session.getAttribute("user");
		if (!folderValidationChecker.validateFolderName(folderName))
			return ResultWrapper.failed("Invalid folder name");

		return folderOpService.createFolder(folderPid, user.getUserId(), folderName);
	}

	/**
	 * 文件夹拷贝
	 * @param destFolderId
	 * @param folderIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/copy", method = RequestMethod.POST)
	public ResultWrapper copyFoldersTo(@RequestParam("destFolderId") String destFolderId,
	                                   @RequestParam("folderIds[]") String[] folderIds,
	                                   HttpSession session)
	{
		String rootFolderId = (String) session.getAttribute("rootFolderId");
		folderIds = FolderValidationChecker.filterRootFolderId(rootFolderId, folderIds);
		if (folderIds.length == 0)
			return ResultWrapper.failed("Invalid folder ids");

		return folderOpService.copyFoldersTo(folderIds, destFolderId);
	}

	/**
	 * 文件夹移动
	 * @param destFolderId
	 * @param folderIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/move", method = RequestMethod.POST)
	public ResultWrapper moveFoldersTo(@RequestParam("destFolderId") String destFolderId,
	                                   @RequestParam("folderIds[]") String[] folderIds,
	                                   HttpSession session)
	{
		String rootFolderId = (String) session.getAttribute("rootFolderId");
		folderIds = FolderValidationChecker.filterRootFolderId(rootFolderId, folderIds);
		if (folderIds.length == 0)
			return ResultWrapper.failed("Invalid folder ids");

		return folderOpService.moveFoldersTo(folderIds, destFolderId);
	}

	/**
	 * 删除文件夹
	 * @param folderIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResultWrapper deleteFolders(@RequestParam("folderIds[]") String[] folderIds,
	                                   HttpSession session)
	{
		String rootFolderId = (String) session.getAttribute("rootFolderId");
		folderIds = FolderValidationChecker.filterRootFolderId(rootFolderId, folderIds);
		if (folderIds.length == 0)
			return ResultWrapper.failed("Invalid folder ids");

		return folderOpService.deleteFolders(folderIds);
	}

	/**
	 * 文件夹重命名
	 * @param folderId
	 * @param newFolderName
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	public ResultWrapper renameFolder(@RequestParam("folderId") String folderId,
	                                  @RequestParam("newFolderName") String newFolderName,
	                                  HttpSession session)
	{
		String rootFolderId = (String) session.getAttribute("rootFolderId");
		if (folderId.equals(rootFolderId))
			return ResultWrapper.failed("Invalid folder id");

		if (!folderValidationChecker.validateFolderName(newFolderName))
			return ResultWrapper.failed("Invalid folder name");

		return folderOpService.renameFolder(folderId, newFolderName);
	}

	/**
	 * 获取文件夹下一级子目录及文件
	 * @param folderId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResultWrapper listSubFoldersAndFiles(@RequestParam("folderId") String folderId)
	{
		return folderOpService.listSubFoldersAndFiles(folderId);
	}

	/**
	 * 获取用户的根目录信息
	 * @param userId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/root", method = RequestMethod.POST)
	public ResultWrapper getRootFolderInfo(@RequestParam("userId") String userId)
	{
		return folderOpService.getRootFolderInfo(userId);
	}

	/**
	 * 获取整个目录树
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/tree", method = RequestMethod.POST)
	public ResultWrapper getFolderTree(
		@RequestParam(value = "xfolderIds[]", required = false) String[] folderIds,
           HttpSession session)
	{
		UserInfo user = (UserInfo) session.getAttribute("user");

		return folderOpService.getFolderStructure(user.getUserId(), folderIds);
	}

	// ================================================================================================

	@Autowired
	public FolderController(FolderOpService folderOpService)
	{
		Assert.notNull(folderOpService);
		this.folderOpService = folderOpService;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		Assert.notNull(applicationContext);

		FolderValidationChecker checker = applicationContext.getBean(FolderValidationChecker.class);
		this.folderValidationChecker = checker != null ? checker : new DefaultFolderValidationChecker();
	}
}
