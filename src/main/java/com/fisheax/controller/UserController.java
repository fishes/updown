/**
 * @FileName UserController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/4/2016
 */
package com.fisheax.controller;

import com.fisheax.component.ResultWrapper;
import com.fisheax.pojo.FolderInfo;
import com.fisheax.pojo.UserInfo;
import com.fisheax.service.FolderOpService;
import com.fisheax.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @ClassName: UserController
 * @Description 用户相关控制器
 * @Author fisheax
 * @Date 11/4/2016
 */
@Controller
@RequestMapping("/user")
public class UserController
{
	private UserService userService;
	private FolderOpService folderOpService;

	@RequestMapping("/login")
	public String login(HttpSession session)
	{
		return session.getAttribute("user") != null ? "home" : "login";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session, SessionStatus status)
	{
		session.removeAttribute("user");
		status.setComplete();
		return "redirect:login";
	}

	@RequestMapping("/register")
	public String register()
	{
		return "register";
	}

	@RequestMapping("/home")
	public String home()
	{
		return "home";
	}

	@RequestMapping("/login/check")
	public String login(@Valid UserInfo info, BindingResult result,
	                    HttpSession session, RedirectAttributes attributes)
	{
		if (result.hasErrors())
		{
			attributes.addFlashAttribute("msg", "username or password can not be blank");
			return "redirect:../login";
		}

		ResultWrapper resultWrapper = userService.login(info);
		if (resultWrapper.succeed())
		{
			UserInfo user = (UserInfo) resultWrapper.data();
			session.setAttribute("user", user);
			session.setAttribute("rootFolderId",
				((FolderInfo) folderOpService.getRootFolderInfo(user.getUserId()).data()).getFolderId());
			return "redirect:../home";
		}

		attributes.addFlashAttribute("msg", "username or password is incorrect");
		return "redirect:../login";
	}

	@RequestMapping("/register/request")
	public String register(@Valid UserInfo info, BindingResult result, RedirectAttributes attributes)
	{
		attributes.addFlashAttribute("msg", result.hasErrors() ? "username or password can not be blank" :
			userService.register(info).succeed() ? "register success" : "register failed");
		return "redirect:../register";
	}

	// ================================================================================================

	@Autowired
	public UserController(UserService userService, FolderOpService folderOpService)
	{
		Assert.notNull(userService);
		Assert.notNull(folderOpService);

		this.userService = userService;
		this.folderOpService = folderOpService;
	}
}
