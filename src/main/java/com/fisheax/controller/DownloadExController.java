/**
 * @FileName DownloadExController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/21/2016
 */
package com.fisheax.controller;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.utils.StringUtil;
import com.fisheax.pojo.UserInfo;
import com.fisheax.service.FileOpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: DownloadExController
 * @Description
 * @Author fisheax
 * @Date 11/21/2016
 */
@RestController
public class DownloadExController
{
	private final String rangePatternString = "^bytes=([0-9]*)-([0-9]*)$";
	private Pattern pattern = Pattern.compile(rangePatternString);
	private FileOpService fileOpService;

	/**
	 * 文件下载, 支持断点续传
	 * @param fileId
	 * @param request
	 * @param response
	 * @param session
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping("/download/{fileId}")
	@SuppressWarnings("unchecked")
	public void readFileChunk(@PathVariable("fileId") String fileId,
	                          HttpServletRequest request, HttpServletResponse response,
	                          HttpSession session) throws ServletException, IOException
	{
		do
		{
			// if client do not support continously transport, then rewrite
			// if client do not use the specific pattern, then rewrite
			String rangeHeader = request.getHeader("Range");
			if (StringUtil.empty(rangeHeader))
				break;

			Matcher matcher = pattern.matcher(rangeHeader);
			if (!matcher.matches())
				break;

			long startPos = -1;
			long endPos = -1;

			try
			{
				startPos = Long.parseLong(matcher.group(1));
				endPos = Long.parseLong(matcher.group(2));
			}
			catch (NumberFormatException ex)
			{
				// no need for judging endPos == -1
				if (startPos == -1)
					break;
			}

			UserInfo user = (UserInfo) session.getAttribute("user");
			ResultWrapper result = fileOpService.downloadFile(user.getUserId(), fileId);

			if (result.succeed())
			{
				HashMap<String, Object> data = ((HashMap<String, Object>) result.data());
				File file = new File((String) data.get("filePath"));

				try(
					RandomAccessFile fileInputStream = new RandomAccessFile(file, "r");
					ServletOutputStream outputStream = response.getOutputStream()
				)
				{
					response.setCharacterEncoding("UTF-8");
					response.setHeader("Content-Type", "application/octet-stream");
					response.setStatus(206);

					if (startPos == -1)
						startPos = file.length() - endPos;
					if (endPos == -1)
						endPos = file.length() - 1;

					int contentLength = ((int) (endPos - startPos + 1));
					response.setContentLength(contentLength);
					response.addHeader("content-range",
						String.format("bytes %d-%d/%d", startPos, endPos, file.length()));

					final int bufferLength = 4 * 1024;
					byte[] buffer = new byte[bufferLength];
					int realRead = -1;
					fileInputStream.seek(startPos);

					while ((realRead = fileInputStream.read(buffer, 0, bufferLength)) != -1)
					{
						if (realRead <= contentLength)
						{
							contentLength -= realRead;
							outputStream.write(buffer, 0, realRead);
						}
						else
						{
							outputStream.write(buffer, 0, contentLength);
							break;
						}
					}

					outputStream.flush();
					return;
				}
				catch (IOException ex)
				{
					// if error, forward to download the entire file
					break;
				}
			}
		} while (false);

		request.getRequestDispatcher("/file/download/" + fileId).forward(request, response);
	}

	// ================================================================================================

	@Autowired
	public DownloadExController(FileOpService fileOpService)
	{
		Assert.notNull(fileOpService);

		this.fileOpService = fileOpService;
	}
}
