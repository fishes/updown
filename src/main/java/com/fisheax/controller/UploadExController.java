/**
 * @FileName UploadExController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/21/2016
 */
package com.fisheax.controller;

import com.fisheax.component.ResultWrapper;
import com.fisheax.service.TransTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName: UploadExController
 * @Description
 * @Author fisheax
 * @Date 11/21/2016
 */
@Controller
public class UploadExController
{
	private TransTaskService transTaskService;

	/**
	 * 接收文件分片
	 * 在上传的数据中还有一个 chunkId 不记得当初是怎么考虑的了
	 * 也许是想做 chunk 字节边界的断点续传吧
	 * 反正后续的处理忽略掉了这个参数
	 * @param taskId
	 * @param startPos
	 * @param chunkSize
	 * @param chunkHash
	 * @param chunk
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResultWrapper receiveChunk(@RequestParam("taskId") String taskId,
	                                  @RequestParam("startPos") long startPos,
	                                  @RequestParam("size") long chunkSize,
	                                  @RequestParam("hash") String chunkHash,
	                                  @RequestParam("chunk") MultipartFile chunk)
	{
		if (startPos < 0 || chunkSize < 0 || chunk == null)
			return ResultWrapper.failed("Invalid chunk info or chunk data");

		return transTaskService.saveChunkToFolder(chunk, startPos,
			chunkSize, taskId, chunkHash);
	}

	// ================================================================================================

	@Autowired
	public UploadExController(TransTaskService transTaskService)
	{
		Assert.notNull(transTaskService);

		this.transTaskService = transTaskService;
	}
}
