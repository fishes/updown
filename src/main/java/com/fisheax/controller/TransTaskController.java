/**
 * @FileName TransTaskController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/27/2016
 */
package com.fisheax.controller;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.checker.FileValidationChecker;
import com.fisheax.pojo.UserInfo;
import com.fisheax.service.TransTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @ClassName: TransTaskController
 * @Description 传输任务相关控制器, 上传
 * @Author fisheax
 * @Date 11/27/2016
 */
@Controller
@RequestMapping("/task")
public class TransTaskController
{
	private TransTaskService transTaskService;
	private FileValidationChecker fileValidationChecker;

	/**
	 * 创建新的上传任务
	 * @param folderId
	 * @param fileName
	 * @param fileSize
	 * @param fileHash
	 * @param session
	 * @return 任务信息
	 */
	@ResponseBody
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public ResultWrapper createTransTask(@RequestParam("folderId") String folderId,
	                                     @RequestParam("fileName") String fileName,
	                                     @RequestParam("fileSize") long fileSize,
	                                     @RequestParam("fileHash") String fileHash,
	                                     HttpSession session)
	{
		if (!fileValidationChecker.validateFileName(fileName))
			return ResultWrapper.failed("Invalid file name");
		if (fileSize < 0)
			return ResultWrapper.failed("Invalid file size");

		String userId = ((UserInfo) session.getAttribute("user")).getUserId();
		return transTaskService.createUploadTask(folderId, fileName, fileHash, fileSize, userId);
	}

	/**
	 * 查询任务信息
	 * @param taskId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	public ResultWrapper getTaskInfo(@RequestParam("taskId") String taskId)
	{
		return transTaskService.queryTaskInfo(taskId);
	}

	/**
	 * 上传任务结束通知
	 * @param taskId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/done", method = RequestMethod.POST)
	public ResultWrapper tellTaskDone(@RequestParam("taskId") String taskId)
	{
		return transTaskService.finishUploadTask(taskId);
	}

	/**
	 * 停止上传任务
	 * @param taskId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/stop", method = RequestMethod.POST)
	public ResultWrapper stopUploadTask(@RequestParam("taskId") String taskId)
	{
		return transTaskService.stopUploadTask(taskId);
	}

	// ================================================================================================

	@Autowired
	public TransTaskController(TransTaskService transTaskService, FileValidationChecker fileValidationChecker)
	{
		Assert.notNull(transTaskService);
		Assert.notNull(fileValidationChecker);

		this.transTaskService = transTaskService;
		this.fileValidationChecker = fileValidationChecker;
	}
}
