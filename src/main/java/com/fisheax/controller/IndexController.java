/**
 * @FileName IndexController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/10/2016
 */
package com.fisheax.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName: IndexController
 * @Description 主页跳转
 * @Author fisheax
 * @Date 11/10/2016
 */
@Controller
public class IndexController
{
	@RequestMapping({"/", "/index"})
	public String index()
	{
		return "redirect:user/login";
	}
}
