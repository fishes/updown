/**
 * @FileName FileController.java
 * @Package com.fisheax.controller;
 * @Author fisheax
 * @Date 11/7/2016
 */
package com.fisheax.controller;

import com.fisheax.component.ResultWrapper;
import com.fisheax.component.checker.FileValidationChecker;
import com.fisheax.component.checker.defaults.DefaultFileValidationChecker;
import com.fisheax.pojo.UserInfo;
import com.fisheax.service.FileOpService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName: FileController
 * @Description
 * @Author fisheax
 * @Date 11/7/2016
 */
@Controller
@RequestMapping("/file")
public class FileController implements ApplicationContextAware
{
	private FileOpService fileOpService;
	private FileValidationChecker fileValidationChecker;

	/**
	 * 用户文件上传
	 * @param folderId
	 * @param files
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResultWrapper uploadFiles(@RequestParam("folderId") String folderId,
	                                 @RequestParam("files") List<MultipartFile> files,
	                                 HttpSession session)
	{
		for (MultipartFile file : files)
		{
			int extDotPos = file.getOriginalFilename().lastIndexOf('.');
			String extName = extDotPos == -1 ? "" : file.getOriginalFilename().substring(extDotPos);

			try
			{
				if (!fileValidationChecker.validateFileName(file.getOriginalFilename()) ||
					!fileValidationChecker.validateFileExtName(extName) ||
					!fileValidationChecker.validateFileContent(file.getBytes()))
					return ResultWrapper.failed("The file is invalid");
			}
			catch (IOException e)
			{
				return ResultWrapper.failed("The file is invalid");
			}
		}

		UserInfo user = (UserInfo) session.getAttribute("user");
		return fileOpService.createFiles(user.getUserId(), folderId, files);
	}

	/**
	 * 用户文件下载
	 * @param fileId
	 * @param response
	 * @param session
	 * @throws IOException
	 */
	@RequestMapping("/download/{fileId}")
	@SuppressWarnings("unchecked")
	public void downloadFiles(@PathVariable("fileId") String fileId,
	                          HttpServletResponse response,
	                          HttpSession session)
	{
		UserInfo user = (UserInfo) session.getAttribute("user");
		ResultWrapper result = fileOpService.downloadFile(user.getUserId(), fileId);

		if (result.succeed())
		{
			HashMap<String, Object> data = ((HashMap<String, Object>) result.data());
			File file = new File((String) data.get("filePath"));

			try(
				FileInputStream fileInputStream = new FileInputStream(file);
				ServletOutputStream outputStream = response.getOutputStream()
			)
			{
				response.setCharacterEncoding("UTF-8");
				response.setHeader("Content-Type", "application/octet-stream");
				response.setHeader("Content-Disposition",
					"attachment; filename=\"" +
						new String(((String) data.get("fileName")).getBytes("UTF-8"), "ISO8859-1") + "\"");

				response.setContentLength(((int) file.length()));

				byte[] buffer = new byte[4 * 1024];
				int realRead = -1;
				while ((realRead = fileInputStream.read(buffer, 0, 4 * 1024)) != -1)
					outputStream.write(buffer, 0, realRead);
				outputStream.flush();
			}
			catch (IOException ex)
			{
				// may just do nothing
			}
		}
	}

	/**
	 * 文件拷贝
	 * @param folderId
	 * @param fileIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/copy", method = RequestMethod.POST)
	public ResultWrapper copyFilesTo(@RequestParam("folderId") String folderId,
	                                 @RequestParam("fileIds[]") String[] fileIds)
	{
		return fileOpService.copyFilesTo(fileIds, folderId);
	}

	/**
	 * 文件移动
	 * @param folderId
	 * @param fileIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/move", method = RequestMethod.POST)
	public ResultWrapper moveFilesTo(@RequestParam("folderId") String folderId,
	                                 @RequestParam("fileIds[]") String[] fileIds)
	{
		return fileOpService.moveFilesTo(fileIds, folderId);
	}

	/**
	 * 删除文件
	 * @param fileIds
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResultWrapper deleteFiles(@RequestParam("fileIds[]") String[] fileIds)
	{
		return fileOpService.deleteFiles(fileIds);
	}

	/**
	 * 重命名文件
	 * @param fileId
	 * @param newFileName
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/rename", method = RequestMethod.POST)
	public ResultWrapper renameFile(@RequestParam("fileId") String fileId,
	                                @RequestParam("newFileName") String newFileName)
	{
		if (!fileValidationChecker.validateFileName(newFileName))
			return ResultWrapper.failed("Invalid file name");

		return fileOpService.renameFile(fileId, newFileName);
	}

	// ================================================================================================

	@Autowired
	public FileController(FileOpService fileOpService)
	{
		Assert.notNull(fileOpService);
		this.fileOpService = fileOpService;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		Assert.notNull(applicationContext);

		FileValidationChecker checker = applicationContext.getBean(FileValidationChecker.class);
		this.fileValidationChecker = checker != null ? checker : new DefaultFileValidationChecker();
	}
}
